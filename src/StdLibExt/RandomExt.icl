implementation module RandomExt

import StdEnvExt
import StdClass, StdInt, StdString
import System.Time

::	RandomSeed	=:	RS Int

predictableRandomSeed :: RandomSeed
predictableRandomSeed
= RS 42

getNewRandomSeed :: !*World -> (!RandomSeed, !*World)
getNewRandomSeed env
# (Timestamp t, env)	= time env
= (RS (t bitand 65535), env)

random :: !RandomSeed -> (!Int,!RandomSeed)
random (RS seed)
= (newSeed,RS newSeed)
where
	newSeed		= if (nextSeed>=0) nextSeed (nextSeed+65537)
	nextSeed	= (seed75 bitand 65535)-(seed75>>16)
	seed75		= seed*75

instance toString   RandomSeed where toString (RS r)      = toString r
instance fromString RandomSeed where fromString str       = RS (fromString str)
instance ==         RandomSeed where (==) (RS r1) (RS r2) = r1 == r2
