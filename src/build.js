const fs=require ('fs');
const path=require ('path');

const copydir=require ('copy-dir');
const packager=require ('./iTasks-electron/src/clean-electron-packager.js');

packager ({
	out: path.join ('..','dist'),
	cleanApp: 'SoccerFun',
	electronApp: 'Start',
	ignore: [
		/* Source code */
		/^\/Game$/,
		/^\/Gui$/,
		/^\/StdLibExt$/,
		/^\/StdReferee$/,
		/^\/StdTeam$/,
		/^\/Teams$/,
		/^\/Util$/,
		/* Miscellaneous files used during development */
		/\/tags$/,
		/\/SoccerFun\.config\.json$/,
		/\/SoccerFun\.jsonl$/,
	],
	afterCopy: (buildPath,version,platform,arch,done) => {
		/* Config */
		const config={
			options: {
				playingTime: 4,
				useRandomSeed: true,
				logging: false
			},
			clean_installation_directory: platform=='win32' ? 'C:\\Clean' : '/opt/clean',
			team_heap_size: 10485760,
			team_stack_size: 1048576,
		};
		fs.writeFileSync (
			path.join (buildPath,'..','..','SoccerFun.config.json'),
			JSON.stringify (config)
		);

		const src_dirs=['Game','StdLibExt','StdReferee','StdTeam'];
		fs.mkdirSync (path.join (buildPath,'..','dev'));
		fs.mkdirSync (path.join (buildPath,'..','dev','Teams'));
		for (var i=0; i<src_dirs.length; i++){
			copydir.sync (
				path.join (__dirname,src_dirs[i]),
				path.join (buildPath,'..','dev',src_dirs[i]),
				{
					filter: (stat,file) => {
						if (stat=='file')
							return file.match (/\.[di]cl$/) != null;
						else
							return path.basename (file) != 'Clean System Files';
					}
				}
			);
		}

		done();
	}
}).then (appPaths => {
	console.log (`Electron app bundles created:\n${appPaths.join('\n')}`);
});
