module SoccerFun

import iTasks

import Electron.App

import Gui.Main

Start :: *World -> *World
Start world = serveElectron
	(\_ electron_opts opts ->
		( electron_opts
		, {opts & persistTasks=False}
		, []
		))
	mainApp
	world
