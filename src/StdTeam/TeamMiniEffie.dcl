definition module TeamMiniEffie

/**	This module implements a team of simple minded football players.
	Use it for testing Soccer-Fun.
*/

import Team

Team_MiniEffies			:: TeamDefinition
base_TeamName_MiniEffie	:: String
