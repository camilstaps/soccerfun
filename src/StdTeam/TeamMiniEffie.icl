implementation module TeamMiniEffie

import StdEnv, Footballer, FootballerFunctions

Team_MiniEffies :: TeamDefinition
Team_MiniEffies =
	{ teamId				= "MiniEffies"
	, teamFunction			= wrapTeamFunction initialSetup
	}

initialSetup :: !Home !FootballField -> Team
initialSetup home field
|	home==West				= westTeam
|	otherwise				= eastTeam
where
	eastTeam				= mirror field westTeam
	westTeam				= [keeper : fielders]
	clubname				= base_TeamName_MiniEffie +++ if (home == West) "W" "E"
	keeper					= MiniEffie clubname home field {zero & px=scale -0.5 field.flength} 1
	fielders				= [  MiniEffie clubname home field {px=scale (-0.5*dx) field.flength,py=scale (0.5*dy) field.fwidth} nr
							  \\ (dx,dy) <- west_positions_fielders
							   & nr      <- [2..]
							  ]
	west_positions_fielders	= [(0.20, 0.40)
							  ,(0.20,-0.40)
							  ,(0.23, 0.00)	
							  ,(0.50, 0.45)
							  ,(0.50,-0.45)
							  ,(0.60, 0.00)
							  ,(0.70, 0.35)
							  ,(0.70,-0.35)
							  ,(0.90, 0.05)
							  ,(0.90,-0.05)
							  ]
							  
base_TeamName_MiniEffie		:: String
base_TeamName_MiniEffie		= "MiniEffies"							  

:: MiniMemory				= { home :: !Home }

MiniEffie :: !ClubName !Home !FootballField !Position !PlayersNumber -> Footballer
MiniEffie club home field position nr
	= { playerID			= {clubName=club,playerNr=nr}
	  , name				= "MiniF." <+++ nr
	  , length				= min_length
	  , pos					= position
	  , nose				= zero
	  , speed				= zero
	  , skills				= (Running, Kicking, Rotating)
	  , effect				= ?None
	  , stamina				= max_stamina
	  , health				= max_health
	  , brain				= { memory = {home=home}, ai = wrapFootballerAI (minibrain field) }
	  }	  

minibrain :: !FootballField !(!BrainInput, !MiniMemory) -> (!BrainOutput,!MiniMemory)
minibrain field (input=:{me,referee}, memory=:{home})
| d <= maxKickReach me = kick (centerOfGoal (other home) field) (input, new_memory)
| otherwise            = track_ball zero (input, new_memory)
where
  d                    = dist me (getBall input)
  new_memory           = if (any isEndHalf referee) {memory & home = other home} memory
