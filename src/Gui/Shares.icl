implementation module Gui.Shares

import System.File
import System.FilePath

import iTasks
import iTasks.Internal.IWorld
import iTasks.Internal.SDS

from Team import :: Team, :: TeamDefinition, allAvailableTeams
from Referee import :: Referee, allAvailableReferees
import Util.Derives

share :: !String a -> SimpleSDSLens a | JSONEncode{|*|}, JSONDecode{|*|}, TC a
share name default = sdsFocus name (memoryStore "SoccerFun" (?Just default))

isRunning :: SimpleSDSLens Bool
isRunning =: share "isRunning" False

theGame :: SimpleSDSLens (?(?(Int,RefereeAction), FootballGame))
theGame =: share "theGame" ?None

theAvailableTeams :: SimpleSDSLens [TeamDefinition]
theAvailableTeams =: share "theAvailableTeams" allAvailableTeams

theAvailableReferees :: SimpleSDSLens [RefereeDefinition]
theAvailableReferees =: share "theAvailableReferees" allAvailableReferees

theInterpretedTeamsDirectory :: SDSSource () FilePath ()
theInterpretedTeamsDirectory =: createReadOnlySDS \() iworld -> get iworld
where
	/* When the app is distributed, the source code resides under
	 * ./resources/dev. Otherwise, we assume that there is a Teams directory
	 * directly in the src directory of the repository, for development. */
	get iworld=:{IWorld|options={appPath},world}
		# dir = takeDirectory appPath
		# resources_dir = dir </> "resources" </> "dev" </> "Teams"
		# (e,world) = fileExists resources_dir world
		# dir = if e resources_dir (dir </> "Teams")
		= (dir, {iworld & world=world})

timer :: SimpleSDSLens Timespec
timer =: sdsFocus {start=zero, interval={tv_sec=0,tv_nsec=20000000}} iworldTimespec
