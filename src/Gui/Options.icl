implementation module Gui.Options

import StdEnv

import Data.Func
import Data.Functor

import iTasks

import Electron.App

import Gui.Field
import Gui.Shares
import Gui.Util

from matchGame import
	:: FootballGame{team1,team2,referee,gameOver,match,options}, :: Options{..}
from matchControl import
	:: Match{playingTime,score,seed,theField,theReferee}, :: PlayingTime(..),
	instance zero PlayingTime, instance < PlayingTime,
	setMatchStart
from matchLog import logMatchStart
from Footballer import
	:: Home(..), :: Minutes,
	class nameOf(..), class minutes(..),
	instance zero Minutes, instance < Minutes, instance minutes Real,
	getDefaultField
import RandomExt
from Referee import
	:: Referee, :: RefereeDefinition,
	instance nameOf Referee, allAvailableReferees, initialReferee
from StdEnvExt import :: Fallible(..)
from Team import
	:: Team, :: TeamDefinition{..},
	instance nameOf Team, allAvailableTeams
import Util.Derives

theOptions :: SimpleSDSLens Options
theOptions =: mapReadWrite
	( \(?Just (_,g)) -> g.options
	, \opts -> fmap \(ref,g) -> ?Just (ref, {g & options=opts})
	)
	?None
	theGame

gameLens :: !(FootballGame -> a) !(a FootballGame -> FootballGame) -> Shared SDSLens (?a)
gameLens read write = mapReadWrite
	( fmap (read o snd)
	, maybe ?Just (\t -> fmap (\(ref,g) -> ?Just (ref, write t g)))
	)
	?None
	theGame

theReferee :: Shared SDSLens (?RefereeDefinition)
theReferee =: gameLens (\m -> m.referee) (\r g -> {g & referee=r})

editOptions :: Task ()
editOptions =
	get theGame >>- \(?Just (_,{FootballGame | team1,team2})) ->
	get theAvailableTeams >>- \available_teams ->
	let
		/* We cannot simply use team1 and team2 from theGame, because the teams
		 * may have been reloaded (in the case of interpreted teams). */
		selected_team1 = case [t \\ t <- available_teams | t.teamId==team1.teamId] of
			[t:_] -> ?Just t
			_     -> ?None
		selected_team2 = case [t \\ t <- available_teams | t.teamId==team2.teamId] of
			[t:_] -> ?Just t
			_     -> ?None
	in
	(
		updateSharedInformation [] theOptions
	-&&-
		(Label "Team 1" @>> editChoiceWithShared
			[ChooseFromDropdown
				\t -> case t.teamFunction West getDefaultField of
					Failed e -> "Failed ("+++e+++")"
					Succeeded t -> nameOf t]
			theAvailableTeams selected_team1)
	-&&-
		(Label "Team 2" @>> editChoiceWithShared
			[ChooseFromDropdown
				\t -> case t.teamFunction East getDefaultField of
					Failed e -> "Failed ("+++e+++")"
					Succeeded t -> nameOf t]
			theAvailableTeams selected_team2)
	-&&-
		(Label "Referee" @>> editSharedChoiceWithShared
			[ChooseFromDropdown \r -> nameOf (initialReferee r getDefaultField)]
			theAvailableReferees theReferee)
	) @ (\(opts,(team1,(team2,_))) -> (opts,team1,team2)) >>*
	[OnAction ActionOk $ ifValue validateOpts cont]
where
	validateOpts ({Options | playingTime},_,_) =
		zero < playingTime && playingTime <= PlayingTime (minutes 90.0)

	cont ({useRandomSeed,logging},team1,team2) =
		accWorld getNewRandomSeed >>- \seed ->
		get theGame >>- \(?Just (_,game)) ->
		updateOptions
			(if useRandomSeed seed predictableRandomSeed)
			team1 team2
			game >>- \game=:{FootballGame | match} ->
		set (?Just (?None,game)) theGame >-|
		if logging
			(appWorld (logMatchStart match))
			(return ()) >-|
		closeWindow

	updateOptions seed teamdef1 teamdef2 game=:{FootballGame | referee,match} =
		failedToException (teamdef1.teamFunction West match.theField) >>- \team1 ->
		failedToException (teamdef2.teamFunction East match.theField) >>- \team2 ->
		return
			{ FootballGame
			| game
			& team1 = teamdef1
			, team2 = teamdef2
			, match = setMatchStart
				team1 team2
				match.theField
				(initialReferee referee match.theField)
				game.options.Options.playingTime
				seed
			, gameOver = False
			}
