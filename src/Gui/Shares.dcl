definition module Gui.Shares

from System.FilePath import :: FilePath
from System.Time import :: Timespec
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.SDS.Definition import :: SDSLens, :: SimpleSDSLens, :: SDSSource

from matchControl import :: Half, :: Score, :: NrOfGoals
from matchGame import :: FootballGame
from Footballer import :: Footballer, :: FootballField, :: Home, :: RefereeAction
from Team import :: TeamDefinition
from Referee import :: RefereeDefinition

share :: !String a -> SimpleSDSLens a | JSONEncode{|*|}, JSONDecode{|*|}, TC a

isRunning :: SimpleSDSLens Bool

theGame :: SimpleSDSLens (?(?(Int,RefereeAction), FootballGame))

theAvailableTeams :: SimpleSDSLens [TeamDefinition]
theAvailableReferees :: SimpleSDSLens [RefereeDefinition]

theInterpretedTeamsDirectory :: SDSSource () FilePath ()

timer :: SimpleSDSLens Timespec
