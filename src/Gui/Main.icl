implementation module Gui.Main

import StdEnv

import Control.Applicative
import Control.Monad => qualified return, forever, sequence
from Data.Func import $
import qualified Data.Map as Map
import Data.Tuple
import System.Directory
import System.FilePath
import System.Time
import qualified Text
from Text import class Text(concat), instance Text String

import iTasks

import ABC.Interpreter
import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import Electron.App
import Electron.Dialog
import Electron.Menu => qualified SelectAll, applicationMenu

import Gui.Config
import Gui.Editor
import Gui.Field
import Gui.InterpretedTeam
import Gui.Options
import Gui.Shares
import Gui.Util

from matchControl import getBeginGame
from matchGame import :: FootballGame{gameOver}
from Team import :: Team, allAvailableTeams
import Util.Derives

derive class iTask DeserializedValue, InterpretedTeamDefinition

gEditor{|InterpretedFootballer|} _ = abort "gEditor{|InterpretedFootballer\}\n"
gEq{|InterpretedFootballer|} _ _ = abort "gEq{|InterpretedFootballer|}\n"
gText{|InterpretedFootballer|} _ _ = abort "gText{|InterpretedFootballer|}\n"
JSONEncode{|InterpretedFootballer|} _ _ = abort "JSONEncode{|InterpretedFootballer|}\n"
JSONDecode{|InterpretedFootballer|} _ _ = abort "JSONDecode{|InterpretedFootballer|}\n"

:: ApplicationMenu =
	{ running :: !Bool
	}

derive class iTask ApplicationMenu

applicationMenu :: SDSLens () () ApplicationMenu
applicationMenu =: mapReadWrite
	( \_ -> ()
	, \menu _ -> ?Just (?Just (toMenu menu))
	)
	?None
	'Electron.Menu'.applicationMenu
where
	toMenu {running} =
		[ menuItem "File" <<@ SubMenu
			[ menuItem "Team Editor" <<@ Accelerator "CmdOrCtrl+E" <<@ OnClick open_editor
			, menuItem "Reload Interpreted Teams" <<@ eord (not running) <<@ OnClick reload_teams
			, MenuSeperator
			, menuItem "Preferences" <<@ eord (not running) <<@ OnClick preferences
			, MenuSeperator
			, menuItem "Quit" <<@ Accelerator "CmdOrCtrl+Q" <<@ Quit
			]
		, menuItem "Game" <<@ SubMenu
			[ menuItem "Options" <<@ Accelerator "CmdOrCtrl+O" <<@ eord (not running) <<@ OnClick options
			, MenuSeperator
			, menuItem "Run / Pause" <<@ Accelerator "CmdOrCtrl+R" <<@ OnClick run_or_pause
			, menuItem "Step" <<@ Accelerator "CmdOrCtrl+S" <<@ OnClick step
			]
		, menuItem "Tools" <<@ SubMenu
			[ menuItem "Open development tools" <<@ Accelerator "Shift+CmdOrCtrl+I" <<@ ToggleDevTools
			]
		]

	eord b = if b Enabled Disabled

	preferences parent =
		createWindow
			{ ElectronWindowOptions
			| defaultOptions
			& parent = ?Just parent
			, task   = \_ -> editConfiguration
			, width  = 300
			, height = 200
			} @!
		()

	options parent =
		createWindow
			{ ElectronWindowOptions
			| defaultOptions
			& parent = ?Just parent
			, task   = \_ -> editOptions
			, width  = 300
			, height = 200
			} @!
		()

	open_editor _ =
		createWindow
			{ ElectronWindowOptions
			| defaultOptions
			& webPreferences = ["webSecurity" :> False]
			, windowModifier = \win ->
				appJS (win .# "autoHideMenuBar" .= True) >>|
				appJS (win .# "setMenuBarVisibility" .$! False)
			, task = \_ -> editorTask
			} @!
		()

	reload_teams _ = reloadInterpretedTeams @! ()
	run_or_pause _ = upd not isRunning @! ()
	step _ = set False isRunning >-| stepGameTask @! ()

mainApp :: Task ()
mainApp =
	set {running=False} applicationMenu >-|

	get theConfiguration >>- \{Configuration | options} ->
	accWorld (getBeginGame options) >>- failedToException >>- \game ->
	set (?Just (?None,game)) theGame >-|

	appendTopLevelTask 'Map'.newMap True playGame >-|
	appendTopLevelTask 'Map'.newMap True watchIsRunning >-|

	createWindow
		{ ElectronWindowOptions
		| defaultOptions
		& task = \_ -> main @! ()
		} @!
	()
where
	main =
		sizeAttr FlexSize FlexSize @>>
		viewSharedInformation [ViewUsing fromJust field] theGame

	playGame :: Task ()
	playGame = onChangeAndKill isRunning \running -> if running run (return ())
	where
		run =
			get timer >>- \now ->
			watch timer >>*
			[ OnValue $ ifValue ((<>) now) \_ ->
				stepGameTask >>- \(_,{gameOver}) ->
				if gameOver (return ()) run
			]

	watchIsRunning :: Task ()
	watchIsRunning =
		onChange isRunning \running ->
		set {running=running} applicationMenu

reloadInterpretedTeams :: Task [TeamDefinition]
reloadInterpretedTeams =
	get theInterpretedTeamsDirectory >>- \dir ->
	accWorldOSError (ensureDirectoryExists dir) >-|
	accWorldOSError (readDirectory dir) >>- \paths ->
	get applicationOptions >>- \{appPath} ->
	get theConfiguration >>- \{team_heap_size,team_stack_size} ->
	let
		bytecodes = [dir </> path \\ path <- paths | takeExtension path == "bc"]
		settings =
			{ defaultDeserializationSettings
			& heap_size  = team_heap_size
			, stack_size = team_stack_size
			, file_io    = False
			}
	in
	allTasks
		[ accWorld (get_start_rule_as_expression_strict settings path appPath) @ tuple path
		\\ path <- bytecodes
		] >>- \mbTeams ->
	let
		teams = [unwrapTeamDefinition team \\ (_,DV_Ok team) <- mbTeams]
		failed_paths = [(path,toString v) \\ (path,v) <- mbTeams | not (v=:DV_Ok _)]
	in
	allTasks [showSimpleMessageBox WarningMsg (warn path err) \\ (path,err) <- failed_paths] >-|
	showSimpleMessageBox InfoMsg (info (length teams)) >-|
	set (allAvailableTeams ++ teams) theAvailableTeams
where
	warn path err = 'Text'.concat5 "Failed to load bytecode from " path ": " err "."
	info n = if (n==1) "1 team has been imported." (toString n+++" teams have been imported.")
