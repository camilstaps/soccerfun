implementation module Gui.Field

import StdEnv

import Control.Monad => qualified return, sequence, forever
import Data.Func => qualified mapSt
import qualified Data.Map
from Text import concat3, concat4
import Text.HTML

import iTasks
import iTasks.Extensions.DateTime
import iTasks.Internal.Serialization

import ABC.Interpreter.JavaScript

import Electron.App => qualified defaultOptions

import Gui.Shares

import matchControl => qualified return, <+++, +++>
import Util.Derives

field :: Editor (?(Int,RefereeAction), FootballGame) ()
field = withClientSideInit initUI $ leafEditorToEditor
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = \_ -> Ok ()
	}

onReset
	:: !UIAttributes (?(?(Int,RefereeAction), FootballGame)) !*VSt
	-> *(*MaybeErrorString *(UI, FootballGame, * ?()), *VSt)
onReset attr (?Just (_,game)) vst
	# ball = getFootball game.match.Match.theBall (game.match.Match.team1 ++ game.match.Match.team2)
	  (encoded_ball,vst) = serializeForClient (hyperstrict ball) vst
	  (encoded_players_1,vst) = encodeTeam game.match.Match.team1 vst
	  (encoded_players_2,vst) = encodeTeam game.match.Match.team2 vst
	=
		( Ok
			( uia UIHtmlView $ 'Data.Map'.union attr $ 'Data.Map'.fromList
				[ ("field", JSONString (toString (toJSON game.match.theField)))
				, ("teams", encodeTeamNames game.match)
				, ("score", encodeScore game.match)
				, ("time", encodeTime game.match.Match.playingTime)
				, ("ball", JSONString encoded_ball)
				, ("players1", encoded_players_1)
				, ("players2", encoded_players_2)
				]
			, game
			, ?None
			)
		, vst
		)

onEdit :: () FootballGame *VSt -> *(*MaybeErrorString *(UIChange, FootballGame, * ?()), *VSt)
onEdit _ st vst = (Ok (NoChange, st, ?None), vst)

onRefresh
	:: !(?(Int,RefereeAction), FootballGame) !FootballGame !*VSt
	-> *(*MaybeErrorString *(UIChange, FootballGame, * ?()), *VSt)
onRefresh (?Just (timeout,action), _) st vst =
	( Ok
		( ChangeUI
			[ SetAttribute "ref-action" $ JSONString $ toString $ toJSON (timeout,action)
			]
			[]
		, st
		, ?None
		)
	, vst
	)
onRefresh (?None,new) old vst
	# ball = getFootball new.match.Match.theBall (new.match.Match.team1 ++ new.match.Match.team2)
	  (encoded_ball,vst) = serializeForClient (hyperstrict ball) vst
	  (encoded_players_1,vst) = encodeTeam new.match.Match.team1 vst
	  (encoded_players_2,vst) = encodeTeam new.match.Match.team2 vst
	=
		( Ok
			( ChangeUI
				[ SetAttribute "time" $ encodeTime new.match.Match.playingTime
				, SetAttribute "ball" (JSONString encoded_ball)
				, SetAttribute "players1" encoded_players_1
				, SetAttribute "players2" encoded_players_2
				: catMaybes [score_change, team_names_change]
				]
				[]
			, new
			, ?None
			)
		, vst
		)
where
	score_change
		| old.match.score <> new.match.score || old.match.Match.playingHalf <> new.match.Match.playingHalf
			= ?Just $ SetAttribute "score" $ encodeScore new.match
			= ?None
	team_names_change
		| old.match.Match.playingHalf <> new.match.Match.playingHalf ||
				nameOf old.match.Match.team1 <> nameOf new.match.Match.team1 ||
				nameOf old.match.Match.team2 <> nameOf new.match.Match.team2
			= ?Just $ SetAttribute "teams" $ encodeTeamNames new.match
			= ?None

:: PositionedPlayer =
	{ playerNr :: !PlayersNumber
	, name     :: !String
	, pos      :: !Position
	, nose     :: !Angle
	}

stripFootballer :: !Footballer -> PositionedPlayer
stripFootballer {playerID=id=:{FootballerID | playerNr},name,pos,nose} =
	{ PositionedPlayer
	| playerNr = playerNr
	, name     = name
	, pos      = pos
	, nose     = nose
	}

encodeTeam :: !Team !*VSt -> (!JSONNode, !*VSt)
encodeTeam team vst
	# (s,vst) = serializeForClient (hyperstrict (map stripFootballer team)) vst
	= (JSONString s, vst)

encodeScore :: !Match -> JSONNode
encodeScore {playingHalf,score=(s1,s2)}
	| playingHalf=:FirstHalf
		= JSONString $ concat3 (toString s1) "-" (toString s2)
		= JSONString $ concat3 (toString s2) "-" (toString s1)

encodeTime :: !Minutes -> JSONNode
encodeTime minutes = JSONString time
where
	time = concat4 (toString min) ":" (if (sec<10) "0" "") (toString sec)
	min = s/60
	sec = s rem 60
	s = toInt (toReal minutes * 60.0)

encodeTeamNames :: !Match -> JSONNode
encodeTeamNames {Match | playingHalf,team1,team2}
	| playingHalf=:FirstHalf
		= JSONArray [JSONString (nameOf team1), JSONString (nameOf team2)]
		= JSONArray [JSONString (nameOf team2), JSONString (nameOf team1)]

initUI _ me w
	# (field,w) = me .# "attributes.field" .?? ("", w)
	  field = fromJust (fromJSON (fromString field))
	# w = addCSSFromUrl "/css/field.css" ?None w
	# (onAttributeChange,w) = jsWrapFun (onAttributeChange` field me) me w
	# w = (me .# "onAttributeChange" .= onAttributeChange) w
	# (initDOMEl,w) = jsWrapFun (initDOMEl field me) me w
	# w = (me .# "initDOMEl" .= initDOMEl) w
	= w
initDOMEl field me _ w
	# w = (me .# "domEl.style.padding" .= 0) w
	# w = (me .# "domEl.innerHTML" .= toString field_svg) w
	# w = seqSt (addAudioTag me) audio_tags w
	# w = onAttributeChange field "score" (me .# "attributes.score") me w
	# w = onAttributeChange field "teams" (me .# "attributes.teams") me w
	# w = onAttributeChange field "time" (me .# "attributes.time") me w
	# w = onAttributeChange field "ball" (me .# "attributes.ball") me w
	# w = onAttributeChange field "players1" (me .# "attributes.players1") me w
	# w = onAttributeChange field "players2" (me .# "attributes.players2") me w
	= w
where
	audio_tags =
		[ "ball-out"
		, "center-kick"
		, "end-game-or-half"
		, "foul-play"
		, "offside"
		, "tackles-etc"
		, "toss-coin"
		, "wrong-position-to-restart"
		]
	addAudioTag me event w
		# (elem,w) = (jsDocument .# "createElement" .$ "audio") w
		# w = (elem .# "classList.add" .$! ("audio-"+++event)) w
		# w = (elem .# "src" .= "/audio/"+++event+++".wav") w
		# w = (elem .# "preload" .= "auto") w
		# w = (me .# "domEl.appendChild" .$! elem) w
		= w

onAttributeChange` field me {[0]=name,[1]=value} w
	= onAttributeChange field (fromJS "" name) value me w
onAttributeChange field attr val me w
	| attr == "time"
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-time") w
		= (elem .# "textContent" .= val) w
	| attr == "ball"
		// Setting the ball also clears the field
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-dynamic") w
		# (new_elem,w) = (elem .# "cloneNode" .$ False) w
		# w = (elem .# "parentNode.replaceChild" .$! (new_elem,elem)) w
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-dynamic-ball") w
		# (new_elem,w) = (elem .# "cloneNode" .$ False) w
		# w = (elem .# "parentNode.replaceChild" .$! (new_elem,elem)) w
		# (ball,w) = jsDeserializeJSVal val w
		= addBall new_elem ball.ballPos w
	| size attr == 8 && attr.[0] == 'p' && attr.[1] == 'l' && attr.[2] == 'a' &&
			attr.[3] == 'y' && attr.[4] == 'e' && attr.[5] == 'r' && attr.[6] == 's'
		# color = if (attr.[7] == '1') "#39c2ce" "#ecef2d"
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-dynamic") w
		# (players,w) = jsDeserializeJSVal val w
		= seqSt (addPlayer color elem) players w
	| attr == "ref-action"
		# (timeout,action) = fromJust (fromJSON (fromString (fromJS "" val)))
		# w = drawRefereeAction timeout action me w
		= maybePlayRefereeActionSound action me w
	| attr == "score"
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-score") w
		= (elem .# "textContent" .= val) w
	| attr == "teams"
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-teamname-west") w
		# w = (elem .# "textContent" .= val .# 0) w
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-teamname-east") w
		= (elem .# "textContent" .= val .# 1) w
	| otherwise
		= jsTrace ("field editor: unknown attribute: "+++attr) w
where
	half_width = toReal field.fwidth / 2.0
	half_length = toReal field.flength / 2.0

	addBall :: !JSVal !Position3D !*JSWorld -> *JSWorld
	addBall container {pxy=pxy=:{px,py},pz} w
		# (circle,w) = createSVGElement "circle"
			[ "stroke" :> "black"
			, "fill"   :> "black"
			, "r"      :> radius
			, "cx"     :> toReal px + half_length
			, "cy"     :> toReal py + half_width
			] w
		= (container .# "appendChild" .$! circle) w
	where
		radius = toReal radius_football * (1.0 + 0.2 * toReal pz)

	addPlayer :: !String !JSVal !PositionedPlayer !*JSWorld -> *JSWorld
	addPlayer color container {PositionedPlayer | playerNr,pos=pos=:{px,py},nose} w
		# (body,w) = createSVGElement "circle"
			[ "stroke-width" :> 0.1
			, "stroke"       :> "black"
			, "fill"         :> color
			, "r"            :> 0.5
			, "cx"           :> player_x
			, "cy"           :> player_y
			] w
		# w = (container .# "appendChild" .$! body) w
		# (nose,w) = createSVGElement "circle"
			[ "stroke-width" :> 0.1
			, "stroke"       :> "black"
			, "fill"         :> color
			, "r"            :> 0.3
			, "cx"           :> nose_x
			, "cy"           :> nose_y
			] w
		= (container .# "appendChild" .$! nose) w
	where
		player_x = toReal px + half_length
		player_y = toReal py + half_width

		nose_dir = {dx=m (cosinus nose), dy=m (sinus nose)}
		nose_pos = move_point (scale 0.8 nose_dir) pos
		nose_x = toReal nose_pos.px + half_length
		nose_y = toReal nose_pos.py + half_width

	drawRefereeAction :: !Int !RefereeAction !JSVal !*JSWorld -> *JSWorld
	drawRefereeAction timeout action me w
		# (text_elem,w) = (me .# "domEl.querySelector" .$ ".field-referee-action") w
		# w = (text_elem .# "textContent" .= showSuccintRefereeAction action) w
		# (elem,w) = (me .# "domEl.querySelector" .$ ".field-referee") w
		# w = (jsWindow .# "clearTimeout" .$! (elem .# "timeout")) w
		# w = (elem .# "style" .# "opacity" .= 1) w
		# (finish,w) = jsWrapFun (\_ -> elem .# "style" .# "opacity" .= 0) me w
		# (timeout,w) = (jsWindow .# "setTimeout" .$ (finish, timeout)) w
		# w = (elem .# "timeout" .= timeout) w
		= w

	maybePlayRefereeActionSound :: !RefereeAction !JSVal !*JSWorld -> *JSWorld
	maybePlayRefereeActionSound action me w
		| size audio_id == 0
			= w
		# (elem,w) = (me .# "domEl.querySelector" .$ (".audio-"+++audio_id)) w
		= (elem .# "play" .$! ()) w
	where
		audio_id = case action of
			Hands _            -> "foul-play"
			TackleDetected _   -> "tackles-etc"
			DangerousPlay _    -> "tackles-etc"
			GameOver           -> "end-game-or-half"
			GameCancelled _    -> "end-game-or-half"
			EndHalf            -> "end-game-or-half"
			Offside _          -> "offside"
			GoalKick _         -> "ball-out"
			Corner _ _         -> "ball-out"
			ThrowIn _ _        -> "ball-out"
			Goal _             -> "center-kick"
			OwnBallIllegally _ -> "wrong-position-to-restart"
			_                  -> ""

stepGameTask :: Task ([RefereeAction], FootballGame)
stepGameTask =
	get theGame >>- \(?Just (_,game)) ->
	accWorld (\w
		# (refActions,game,w) = stepGame game w
		-> ((refActions,game),w)) >>-
	\result=:(refActions,game=:{FootballGame | match=match=:{Match | team1,team2,theBall,playingTime}}) ->
	displayRefActions (filter shouldBeShown refActions) game >-|
	if game.gameOver (set False isRunning @! result) (return result)
where
	shouldBeShown :: !RefereeAction -> Bool
	shouldBeShown action = not
		(  isContinueGame action
		|| isDisplacePlayers action
		|| isDirectFreeKick action
		|| isCenterKick action
		|| isPauseGame action
		|| isAddTime action
		)

	displayRefActions :: ![RefereeAction] !FootballGame -> Task ()
	displayRefActions [] game =
		set (?Just (?None, game)) theGame @! ()
	displayRefActions refActions game =
		set (?Just (?None, game)) theGame >-| // first update the game itself, then show actions
		sequence (map display refActions) @! ()
	where
		display action =
			set (?Just (?Just (1800, action), game)) theGame >-|
			waitForTimer False 2

SVG_NS :== "http://www.w3.org/2000/svg"

createSVGElement :: !String ![(String,JSVal)] !*JSWorld -> (!JSVal, !*JSWorld)
createSVGElement tag attrs w
	# (elem,w) = (jsDocument .# "createElementNS" .$ (SVG_NS, tag)) w
	# w = seqSt (addAttribute elem) attrs w
	= (elem, w)
where
	addAttribute elem (attr,val) w = (elem .# "setAttributeNS" .$! (jsNull, attr, val)) w

// TODO: remove hard-coded strings; build the SVG using values defined in Game/Footballer
field_svg :: HtmlTag
field_svg = SvgTag [ClassAttr "field-container"]
	[ ViewBoxAttr "0" "0" "110" "75"
	, PreserveAspectRatioAttr ?None (?Just XMidYMid) (?Just SVGMeet)
	]
	[ DefsElt [] []
		[ LinearGradientElt [IdAttr "infoboard-gradient"]
			[ X1Attr (SVGLength "0" PT)
			, X2Attr (SVGLength "0" PT)
			, Y1Attr (SVGLength "0" PT)
			, Y2Attr (SVGLength "1" PT)
			]
			[ StopElt [] [StopColorAttr "#e6eced", OffsetAttr "0%"]
			, StopElt [] [StopColorAttr "#b5bebf", OffsetAttr "100%"]
			]
		]
	, SVGElt [ClassAttr "field-infoboard", WidthAttr "6%", HeightAttr "4%"]
		[ XAttr (SVGLength "6" PERCENT)
		, YAttr (SVGLength "1" PERCENT)
		, FillAttr (PaintFuncIRI (IRI "#infoboard-gradient") ?None)
		]
		[ RectElt [] []
		, TextElt [ClassAttr "field-time"]
			[ XAttr (SVGLength "50" PERCENT)
			, YAttr (SVGLength "70" PERCENT)
			, TextAnchorAttr "middle"
			]
			""
		]
	, SVGElt [ClassAttr "field-infoboard field-scoreboard", WidthAttr "60%", HeightAttr "4%"]
		[ XAttr (SVGLength "20" PERCENT)
		, YAttr (SVGLength "1" PERCENT)
		, FillAttr (PaintFuncIRI (IRI "#infoboard-gradient") ?None)
		]
		[ RectElt [] []
		, SVGElt [WidthAttr "40%", HeightAttr "100%"]
			[ ViewBoxAttr "0" "0" "300" "20"
			]
			[ TextElt [ClassAttr "field-teamname-west"]
				[ XAttr (SVGLength "100" PERCENT)
				, YAttr (SVGLength "100" PERCENT)
				, TextAnchorAttr "end"
				]
				""
			]
		, SVGElt [WidthAttr "20%", HeightAttr "100%"]
			[ XAttr (SVGLength "40" PERCENT)
			, ViewBoxAttr "0" "0" "100" "20"
			]
			[ TextElt [ClassAttr "field-score"]
				[ XAttr (SVGLength "50" PERCENT)
				, YAttr (SVGLength "100" PERCENT)
				, TextAnchorAttr "middle"
				]
				""
			]
		, SVGElt [WidthAttr "40%", HeightAttr "100%"]
			[ XAttr (SVGLength "60" PERCENT)
			, ViewBoxAttr "0" "0" "300" "20"
			]
			[ TextElt [ClassAttr "field-teamname-east"]
				[ XAttr (SVGLength "0" PERCENT)
				, YAttr (SVGLength "100" PERCENT)
				]
				""
			]
		]
	, RectElt [WidthAttr "88%", HeightAttr "88%"]
		[ XAttr (SVGLength "6" PERCENT)
		, YAttr (SVGLength "6" PERCENT)
		, FillAttr (PaintColor (SVGColorText "transparent") ?None)
		]
	, SVGElt [ClassAttr "field-field", WidthAttr "88%", HeightAttr "88%"]
		[ XAttr (SVGLength "6" PERCENT)
		, YAttr (SVGLength "6" PERCENT)
		, ViewBoxAttr "0" "0" "110" "75"
		]
		[ LineElt [] // Middle
			[ X1Attr (SVGLength "50" PERCENT), Y1Attr (SVGLength "0" PERCENT)
			, X2Attr (SVGLength "50" PERCENT), Y2Attr (SVGLength "100" PERCENT)
			]
		, CircleElt [] // Center circle
			[ CxAttr (SVGLength "50" PERCENT), CyAttr (SVGLength "50" PERCENT)
			, RAttr (SVGLength "9.15" PERCENT)
			, FillAttr (PaintColor (SVGColorText "transparent") ?None)
			]
		, CircleElt []
			[ CxAttr (SVGLength "50" PERCENT), CyAttr (SVGLength "50" PERCENT)
			, RAttr (SVGLength "0.3" PERCENT)
			, FillAttr (PaintColor (SVGColorText "white") ?None)
			]
		, DefsElt [] []
			[ SVGElt [IdAttr "penalty-area"] [ViewBoxAttr "0" "0" "110" "75"]
				// goal
				[ LineElt []
					[ X1Attr (SVGLength "0"   PERCENT), Y1Attr (SVGLength "54.88" PERCENT)
					, X2Attr (SVGLength "0.5" PERCENT), Y2Attr (SVGLength "54.88" PERCENT)
					]
				, LineElt []
					[ X1Attr (SVGLength "0"   PERCENT), Y1Attr (SVGLength "45.12" PERCENT)
					, X2Attr (SVGLength "0.5" PERCENT), Y2Attr (SVGLength "45.12" PERCENT)
					]
				// penalty area
				, RectElt [WidthAttr "15%", HeightAttr "53.76%"]
					[ YAttr (SVGLength "23.12" PERCENT)
					, FillAttr (PaintColor (SVGColorText "transparent") ?None)
					]
				// goal area
				, RectElt [WidthAttr "5%", HeightAttr "24.42%"]
					[ YAttr (SVGLength "37.79" PERCENT)
					, FillAttr (PaintColor (SVGColorText "transparent") ?None)
					]
				// penalty spot
				, CircleElt []
					[ CxAttr (SVGLength "10" PERCENT), CyAttr (SVGLength "50" PERCENT)
					, RAttr (SVGLength "0.2" PERCENT)
					, FillAttr (PaintColor (SVGColorText "white") ?None)
					]
				// penalty arc
				, RawElt "<path d=\"M 16.5 44.812 a 9.15 9.15 0 0 0 0 -14.624\" fill=\"transparent\" />"
				]
			, SVGElt [IdAttr "corner"] [ViewBoxAttr "0" "0" "110" "75"]
				[RawElt "<path d=\"M 0 0.9 A 1 1 0 0 0 0.9 0\" fill=\"transparent\" />"]
			]

		, RawElt "<use xlink:href=\"#penalty-area\" />"
		, RawElt "<use xlink:href=\"#penalty-area\" transform=\"scale(-1,1)\" transform-origin=\"center\" />"

		, RawElt "<use xlink:href=\"#corner\" />"
		, RawElt "<use xlink:href=\"#corner\" transform=\"scale(-1, 1)\" transform-origin=\"center\" />"
		, RawElt "<use xlink:href=\"#corner\" transform=\"scale( 1,-1)\" transform-origin=\"center\" />"
		, RawElt "<use xlink:href=\"#corner\" transform=\"scale(-1,-1)\" transform-origin=\"center\" />"

		, GElt [ClassAttr "field-dynamic"] [] []
		, GElt [ClassAttr "field-dynamic-ball"] [] [] // the ball must always be on top
		]
	, SVGElt [ClassAttr "field-infoboard field-referee", WidthAttr "60%", HeightAttr "4%"]
		[ XAttr (SVGLength "20" PERCENT)
		, YAttr (SVGLength "95" PERCENT)
		, FillAttr (PaintFuncIRI (IRI "#infoboard-gradient") ?None)
		]
		[ RectElt [] []
		, SVGElt [WidthAttr "100%", HeightAttr "100%"] [ViewBoxAttr "0" "0" "700" "20"]
			[ TextElt [ClassAttr "field-referee-action"]
				[ XAttr (SVGLength "50" PERCENT)
				, YAttr (SVGLength "90" PERCENT)
				, TextAnchorAttr "middle"
				]
				""
			]
		]
	]
