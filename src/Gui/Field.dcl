definition module Gui.Field

from iTasks.UI.Editor import :: Editor
from iTasks.WF.Definition import :: Task

import Electron.App

from Footballer import :: RefereeAction
from matchGame import :: FootballGame

field :: Editor (?(Int,RefereeAction), FootballGame) ()

stepGameTask :: Task ([RefereeAction], FootballGame)
