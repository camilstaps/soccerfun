definition module Gui.Util

from StdClass import class Eq
from StdOverloaded import class ==

from Data.GenEq import generic gEq
from Data.Map import :: Map
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.SDS.Definition import class Identifiable, class Readable,
	class Registrable, class Modifiable, class Writeable, class RWShared,
	:: SDSLens
from iTasks.UI.Editor import :: Editor
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Combinators.Core import :: ParallelTask, :: SharedTaskList
from iTasks.WF.Definition import :: Task, :: TaskAttributes, :: TaskId,
	:: TaskListItem, :: TaskListFilter, class iTask

from StdEnvExt import :: Fallible

failedToException :: !(Fallible a) -> Task a

/**
 * Watch a share. When the share changes value, perform a task in the
 * background.
 * @param The share to watch.
 * @param The task to perform in the background.
 * @result This task never yields a value.
 */
onChange :: !(sds () r w) !(r -> Task a) -> Task () | iTask, Eq r & iTask a & Registrable sds & TC w

/**
 * Like `onChange`, but does the task once at startup as well.
 */
doAndRepeatOnChange :: !(sds () r w) !(r -> Task a) -> Task () | iTask, Eq r & iTask a & Registrable sds & TC w

/**
 * Watch a share. Upon notification, perform a task. The task is terminated and
 * a new instance is created on the next notification.
 * @param The share to watch.
 * @param The task to perform.
 * @result The return value of the task is exposed as an unstable value.
 */
onChangeAndKill :: !(sds () r w) !(r -> Task a) -> Task a | iTask r & iTask a & RWShared sds & TC w

/**
 * Update a share when the value of an `Int` share changes.
 */
writeOnChange :: (sds () Int Int) (sds2 () a a) -> SDSLens () (Int, a) (Int, a) | RWShared sds & RWShared sds2 & TC a

/**
 * Create a parallel task that removes itself from its list when done.
 */
removeWhenStable :: !(Task a) -> ParallelTask b | iTask a & iTask b
