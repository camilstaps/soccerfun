definition module Gui.InterpretedTeam

from ABC.Interpreter import :: DeserializedValue

from Footballer import
	:: Angle, :: Brain, :: BrainInput, :: BrainOutput, :: FallibleFootballerAI,
	:: FootballerAction, :: FootballerEffect, :: FootballerID,
	:: FootballField, :: Health, :: Home, :: Length, :: MajorSkills, :: Metre,
	:: Position, :: Skill, :: Speed, :: Stamina
from StdEnvExt import :: Fallible
from Team import :: TeamDefinition

:: InterpretedFallibleFootballerAI memory :==
	(BrainInput,memory) -> DeserializedValue (Fallible (BrainOutput,memory))

:: InterpretedFootballer = E.memory:
	{ i_playerID :: !FootballerID
	, i_name     :: !String
	, i_length   :: !Length
	, i_pos      :: !Position
	, i_speed    :: !Speed
	, i_nose     :: !Angle
	, i_skills   :: !MajorSkills
	, i_effect   :: !?FootballerEffect
	, i_stamina  :: !Stamina
	, i_health   :: !Health
	, i_brain    :: !Brain (InterpretedFallibleFootballerAI memory) memory
	}

:: InterpretedTeamDefinition =
	{ i_teamId       :: !String
	, i_teamFunction :: !Home FootballField -> DeserializedValue (Fallible [InterpretedFootballer])
	}

unwrapTeamDefinition :: !InterpretedTeamDefinition -> TeamDefinition
