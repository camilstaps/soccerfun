implementation module Gui.Config

import Control.Applicative
import Control.Monad => qualified return, forever, sequence
from Data.Func import $
import Data.Functor

import iTasks => qualified return, <+++, +++>

import Electron.App => qualified defaultOptions

import Gui.Shares
import Gui.Util

import matchGame
import Util.Derives

derive class iTask Configuration

instance == Configuration where (==) a b = a === b

configurationFromGame :: !Configuration !FootballGame -> Configuration
configurationFromGame config {FootballGame | options} =
	{ Configuration
	| config
	& options = options
	}

defaultConfiguration :: Configuration
defaultConfiguration =
	{ Configuration
	| options                      = defaultOptions
	, clean_installation_directory = ""
	, team_heap_size               = 10 << 20 // 10MB
	, team_stack_size              = (512 << 10) << 1 // 512KB for both A and B/C-stack
	}

configurationFile :: String
configurationFile = "SoccerFun.config.json"

theConfiguration :: SimpleSDSLens Configuration
theConfiguration =: mapReadWrite
	( \(default,game) -> maybe default (configurationFromGame default) game
	, \config (_,g) -> ?Just
		( config
		, (\g -> {FootballGame | g & options=config.Configuration.options}) <$> g
		)
	)
	?None
	(storedConfiguration >*< theGame`)
where
	theGame` :: SimpleSDSLens (?FootballGame)
	theGame` = mapReadWrite
		( fmap snd
		, \mbNewGame mbOldGame -> ?Just case mbNewGame of
			?None         -> ?None
			?Just newGame -> ?Just (maybe ?None fst mbOldGame, newGame)
		)
		?None
		theGame

storedConfiguration :: SimpleSDSLens Configuration
storedConfiguration =: mapReadWrite
	( fromMaybe defaultConfiguration
	, \new _ -> ?Just (?Just new)
	)
	?None
	(sdsFocus configurationFile jsonFileShare)

:: EditableConfiguration =
	{ clean_installation_directory :: !FilePath
	, team_heap_size               :: !MemorySize
	, team_stack_size              :: !MemorySize
	}

:: MemorySize =: MemorySize Int
fromMemorySize (MemorySize sz) = sz

gEditor{|MemorySize|} ViewValue = gEditor{|*|} ViewValue
gEditor{|MemorySize|} EditValue =
	mapEditorWrite join $
	withDynamicHintAttributesWithError "amount of bytes" $
	mapEditorRead fromMemorySize $
	mapEditorWrite check $
	integerField
where
	check ?None = Ok ?None
	check (?Just sz)
		| sz rem 8 <> 0 = Error "must be a multiple of 8"
		| sz < 1048576  = Error "must be at least 1048576 (1MB)"
		| sz > 41943040 = Error "must be smaller than 41943040 (40MB)"
		| otherwise     = Ok (?Just (MemorySize sz))

derive gEq MemorySize
derive gText MemorySize
derive JSONEncode MemorySize
derive JSONDecode MemorySize

derive class iTask EditableConfiguration

theEditableConfiguration :: SimpleSDSLens EditableConfiguration
theEditableConfiguration =: mapReadWrite
	( \{Configuration | clean_installation_directory,team_heap_size,team_stack_size} ->
		{ EditableConfiguration
		| clean_installation_directory = clean_installation_directory
		, team_heap_size               = MemorySize team_heap_size
		, team_stack_size              = MemorySize team_stack_size
		}
	, \{EditableConfiguration | clean_installation_directory,team_heap_size,team_stack_size} config -> ?Just
		{ Configuration
		| config
		& clean_installation_directory = clean_installation_directory
		, team_heap_size               = fromMemorySize team_heap_size
		, team_stack_size              = fromMemorySize team_stack_size
		}
	)
	?None
	theConfiguration
where
	default =
		{ EditableConfiguration
		| clean_installation_directory = IF_WINDOWS "C:\\Clean" "/opt/clean"
		, team_heap_size               = MemorySize (10 << 20)
		, team_stack_size              = MemorySize ((512 << 10) << 1)
		}

editConfiguration :: Task ()
editConfiguration =
	updateSharedInformation [] theEditableConfiguration >>*
	[OnAction ActionOk $ hasValue $ const closeWindow]
