implementation module Gui.InterpretedTeam

import ABC.Interpreter

import Footballer

unwrap :: !(DeserializedValue (Fallible a)) -> Fallible a
unwrap (DV_Ok a) = a
unwrap err       = Failed (toString err)

unwrapFootballer :: !InterpretedFootballer -> Footballer
unwrapFootballer player =
	{ playerID = player.i_playerID
	, name     = player.i_name
	, length   = player.i_length
	, pos      = player.i_pos
	, speed    = player.i_speed
	, nose     = player.i_nose
	, skills   = player.i_skills
	, effect   = player.i_effect
	, stamina  = player.i_stamina
	, health   = player.i_health
	, brain    = unwrapBrain player.i_brain
	}
where
	unwrapBrain :: !(Brain (InterpretedFallibleFootballerAI memory) memory) -> Brain (FallibleFootballerAI memory) memory
	unwrapBrain brain =
		{ ai     = \input -> unwrap (brain.ai input)
		, memory = brain.memory
		}

unwrapTeamDefinition :: !InterpretedTeamDefinition -> TeamDefinition
unwrapTeamDefinition team =
	{ teamId       = team.i_teamId
	, teamFunction = \home field -> case unwrap (team.i_teamFunction home field) of
		Succeeded team -> Succeeded (map unwrapFootballer team)
		Failed e       -> Failed e
	}
