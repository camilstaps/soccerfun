implementation module Gui.Util

import Data.Func
import qualified Data.Map as Map

import iTasks

from StdEnvExt import :: Fallible(..)

failedToException :: !(Fallible a) -> Task a
failedToException (Failed e) = throw e
failedToException (Succeeded r) = return r

onChange :: !(sds () r w) !(r -> Task a) -> Task () | iTask, Eq r & iTask a & Registrable sds & TC w
onChange sds f = get sds >>- \v -> task v
where
	task v =
		watch sds >>*
		[ OnValue $ ifValue ((<>) v) \r ->
			appendTopLevelTask 'Map'.newMap True (f r) >-|
			task r
		]

doAndRepeatOnChange :: !(sds () r w) !(r -> Task a) -> Task () | iTask, Eq r & iTask a & Registrable sds & TC w
doAndRepeatOnChange sds f =
	appendTopLevelTask 'Map'.newMap True (get sds >>- f) >-|
	onChange sds f

onChangeAndKill :: !(sds () r w) !(r -> Task a) -> Task a | iTask r & iTask a & RWShared sds & TC w
onChangeAndKill sds f = whileUnchanged sds \v -> f v @? toUnstable
where
	toUnstable v = case v of
		Value v _
			-> Value v False
			-> v

writeOnChange :: (sds () Int Int) (sds2 () a a) -> SDSLens () (Int, a) (Int, a) | RWShared sds & RWShared sds2 & TC a
writeOnChange incrementer share = mapReadWrite
	( id
	, \(j,val) (i,_) -> if (i==j) ?None (?Just (j, val))
	)
	?None
	(incrementer >*< share)

removeWhenStable :: !(Task a) -> ParallelTask b | iTask a & iTask b
removeWhenStable t = \list ->
	t >>*
	[ OnValue $ ifStable $ \_ -> get (taskListSelfId list) >>- \id -> removeTask id list @? const NoValue
	]
