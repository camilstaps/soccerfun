implementation module Gui.Editor

import StdEnv

import Control.Monad => qualified return, forever, sequence
import Data.Func
import Data.Functor
import qualified Data.Map
import Data.Tree
import System.Directory
import System.File
import System.FilePath
import System.OS
import qualified Text
from Text import class Text, instance Text String
import Text.HTML

import iTasks
import iTasks.Extensions.Editors.Monaco
import iTasks.Extensions.Editors.ScrollingTextView
from iTasks.Extensions.Files import selectFileTreeWithShared
import iTasks.Extensions.Process
import iTasks.Internal.SDS

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import Electron.App

import Gui.Config
import Gui.Shares
import Gui.Util

derive JSONEncode FileInfo

derive gHash RTree, FileInfo

openTask :: !String !(Task a) !(SharedTaskList b) -> Task () | iTask a & iTask b
openTask name task list =
	get (taskListMeta list) @ (map \i -> i.TaskListItem.taskAttributes) >>- \attribute_sets ->
	let
		order = 1 + foldr max 0
			[maybe 0 (\(JSONInt i) -> i) ('Data.Map'.get "order" attrs) \\ attrs <- attribute_sets]
		already_opened = not $ isEmpty
			[ ()
			\\ attrs <- attribute_sets
			 , this_name <- maybeToList ('Data.Map'.get "name" attrs)
			 | this_name == JSONString name
			]
		decorated_task =
			("order", JSONInt order) @>>
			("name", JSONString name) @>>
			task
	in
	if (size name > 0 && already_opened)
		(upd (bringToFront order) (taskListMeta list) @! ())
		(appendTask Embedded (removeWhenStable decorated_task) list >-|
			upd (bringToFront order) (taskListMeta list) @! ())
where
	bringToFront order items =
		[
			( taskId
			, if ('Data.Map'.get "name" taskAttributes == ?Just (JSONString name))
				('Data.Map'.singleton "order" (JSONInt order))
				'Data.Map'.newMap
			)
		\\ i=:{TaskListItem | taskId,taskAttributes} <- items
		]

fileTree :: !(SharedTaskList ()) -> Task ()
fileTree taskList =
	get theInterpretedTeamsDirectory >>- \team_dir ->
	accWorldOSError (ensureDirectoryExists team_dir) >-|
	let dir = takeDirectory team_dir in
	ScrollContent @>>
	((widthAttr FlexSize @>>
		selectFileTreeWithShared False dir directoryTreeModifier) >^*
	[ OnAction (Action "Open") (ifValue validPath (openFile dir))
	, OnAction ActionNew (always (newTeam team_dir))
	]) @!
	()
where
	extensions = ["dcl","icl","prj"]

	directoryTreeModifier = createReadOnlySDS \tree iworld -> (modTree True tree, iworld)
	where
		modTree is_root (RNode (path,info) children) = RNode
			(path,info,False,is_root || dropDirectory path == "Teams")
			(sortBy
				((<) `on` \(RNode (p,_,_,_) _) -> p)
				[c \\ c=:(RNode (_,Ok info,_,_) cs) <- children` | not (info.directory && isEmpty cs)])
		where
			children` = [modTree False c \\ c=:RNode (path,Ok info) _ <- children | include path info]

		include path info
			# path = dropDirectory path
			| info.directory
				= path <> "Clean System Files"
				= isMember (takeExtension path) extensions

	validPath [] = False
	validPath [path:_] = isMember (takeExtension path) extensions

	openFile dir [path:_] = openTask path (editor path name taskList) taskList
	where
		name = path % (size dir+1,size path-1)

	newTeam dir = tune (Title "New team") $
		Title "Create a new team" @>>
		(
			(Hint "Your team name:" @>> enterInformation [])
		-&&-
			(Hint "A unique identifier (e.g. your student number(s)):" @>> enterInformation [])
		) >>*
		[ OnAction ActionCancel   $ always $ return ()
		, OnAction ActionContinue $ hasValue (createNewTeam dir)
		]
	createNewTeam dir (name,ident) =
		accWorld (fileExists prj_path) >>- \prj_exists ->
		accWorld (fileExists icl_path) >>- \icl_exists
			| prj_exists || icl_exists ->
				Title "Error" @>>
				viewInformation [] "A team with that identifier already exists." >>*
				[ OnAction (Action "Back") $ always $ newTeam dir // TODO: prefill the input fields with the old info
				]
			| otherwise ->
				accWorldError (writeFile prj_path (defaultProjectFile ident)) id >-|
				accWorldError (writeFile icl_path (defaultTeamImplementation ident name)) id >-|
				openFile dir [icl_path]
	where
		team_name = "Team_"+++ident
		icl_name = team_name+++".icl"
		icl_path = dir </> icl_name
		prj_path = dir </> team_name+++".prj"

editor :: !String !String !(SharedTaskList ()) -> Task ()
editor path name taskList =
	tune (Title name) $
	tune (ApplyLayout layout) $
	get (sdsFocus path fileShare) @ fromMaybe "" >>- \contents ->
	withShared
		{ contents            = contents
		, has_unsaved_changes = False
		, save_requested      = False
		, compile_requested   = False
		} \contentShare ->
	parallel
		[ (Embedded, \list -> onChangeAndKill contentShare (onChangeValue list contentShare))
		, (Embedded, \_ -> updateSharedInformation [] contentShare @! ())
		, (Embedded, \_ -> viewSharedInformation [ViewAs unsavedChangesView] contentShare @! ())
		]
		[ OnAction (Action "Save") $ always (Embedded, removeWhenStable (saveFile contentShare))
		, OnAction (Action "Compile") $ always (Embedded, removeWhenStable (startCompile contentShare))
		] >>*
	[ OnAction ActionClose $ always $ return ()
	]
where
	// We remove step-actions to avoid a button bar being created for the step;
	// the Close button is lifted to the tab. On the side-step we replace
	// parallel-actions with step-actions to get a button bar instead of a
	// toolbar.
	layout = sequenceLayouts
		[ modifyUIAttributes (SelectKeys ["class"]) (removeClassAttr "step-actions")
		, layoutSubUIs SelectChildren $ modifyUIAttributes (SelectKeys ["class"])
			(addClassAttr "step-actions" o removeClassAttr "parallel-actions")
		]

	onChangeValue list share val
		| val.save_requested =
			saveFile share >-|
			upd (\v -> {v & save_requested=False, has_unsaved_changes=False}) share >>-
			onChangeValue list share
		| val.compile_requested =
			startCompile share >-|
			upd (\v -> {v & compile_requested=False}) share >>-
			onChangeValue list share
		| otherwise =
			upd (mbSetAsterisk val.has_unsaved_changes o snd) (sdsFocus fullTaskListFilter taskList) @!
			()
	where
		// TODO: this does not work yet; see https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK/-/issues/432
		mbSetAsterisk needs_asterisk items =
			[ (taskId, if ('Data.Map'.get "name" taskAttributes == ?Just (JSONString path))
				('Data.Map'.singleton TITLE_ATTRIBUTE (JSONString $ if needs_asterisk "*" "" +++ name))
				'Data.Map'.newMap)
			\\ {TaskListItem | taskId,taskAttributes} <- items
			]

	saveFile contentsShare =
		get contentsShare >>- \val ->
		set (?Just val.contents) (sdsFocus path fileShare) @!
		()

	startCompile share =
		get share >>- \val
			| val.has_unsaved_changes ->
				tune InFloatingWindow $
				viewInformation [] "This file has unsaved changes. Save them before compiling." >>*
				[ OnAction ActionClose $ always $ return ()
				]
			| otherwise ->
				openTask ""
					(Title "Compiler" @>> (compile path >>* [OnAction ActionClose $ always $ return ()]))
					taskList

	unsavedChangesView val = if val.has_unsaved_changes
		(Html "There are unsaved changes.")
		(Html "&nbsp;")

compile :: !FilePath -> Task ()
compile path =
	get theConfiguration >>- \{clean_installation_directory} ->
	catchAll (
		(ApplyLayout removeCallProcessUI @>> callProcess
			[ViewUsing processInfoMap processInfoEditor]
			(cpm clean_installation_directory)
			[addExtension (dropExtension (dropDirectory path)) "prj"]
			(?Just (takeDirectory path))
			?None) >>-
		viewInformation
			[ViewUsing processInfoMap processInfoEditor] @!
		()
	)
		(\e -> viewInformation [] e <<@ Title "Exception" @! ())
where
	cpm home = IF_WINDOWS (home </> "cpm.exe") (home </> "bin" </> "cpm")

	removeCallProcessUI = layoutSubUIs
		(SelectByContains (SelectByPath [1])) // when the process has finished, it has no UI any more
		(removeSubUIs (SelectByPath [0]))

	processInfoMap :: ProcessInformation -> (String,ProcessStatus)
	processInfoMap pi = (combine pi.stdout pi.stderr, pi.status)
	where
		combine out err = if (size err==0) out (out+++"\n\n"+++err)

	processInfoEditor :: Editor (String,ProcessStatus) ()
	processInfoEditor = mapEditorWrite (const ()) $ styleAttr "height:100%" @>> container2
		(styleAttr pre_style @>> scrollingTextView)
		(mapEditorRead (\st -> (st, st)) $ toolbar2
			(mapEditorRead statusToIcon   icon)
			(mapEditorRead statusToString textView))
	where
		pre_style = 'Text'.join ";"
			[ "box-sizing: border-box"
			, "flex-grow: 1"
			, "font-family: monospace"
			, "font-size: 90%"
			, "overflow-x:auto"
			, "white-space: pre-wrap"
			, "width: 100%"
			]

		statusToIcon status = case status of
			RunningProcess     -> ("icon-refresh",?Just (statusToString status))
			CompletedProcess 0 -> ("icon-ok",     ?Just (statusToString status))
			CompletedProcess _ -> ("icon-invalid",?Just (statusToString status))
		statusToString status = case status of
			RunningProcess     -> "Compiling..."
			CompletedProcess 0 -> "Compilation succeeded."
			CompletedProcess _ -> "Compilation failed!"

theFileToCompile :: SimpleSDSLens (String, Int)
theFileToCompile =: share "theFileToCompile" ("", 0)

editorTask :: Task ()
editorTask =
	// min-width: 0 is needed to let the monaco editor resize properly (flexboxes have min-width: auto by default)
	AddCSSClass "no-min-width" @>>
	ApplyLayout (layoutSubUIs (SelectByPath [1]) toTabs) @>>
	ArrangeWithSideBar 0 LeftSide True @>>
	parallel
		[ (Embedded, fileTree)
		, (Embedded, \_ -> explanation)
		]
		[]
	@! ()
where
	toTabs = sequenceLayouts
		[ arrangeWithTabs False // TODO: True here breaks the compiler output, due to https://gitlab.science.ru.nl/clean-and-itasks/iTasks-SDK/-/issues/396?
		, setUIAttributes (styleAttr "min-width:0;")
		]

	explanation = tune (Title "Welcome") $
		viewInformation []
			(DivTag []
				[ H1Tag [] [Text "SoccerFun team editor"]
				, PTag []
					[ Text "SoccerFun lets you develop teams within the application. "
					, Text "The teams are compiled in the background and interpreted when they play."
					]
				, PTag []
					[ Text "To get started, click New in the panel on the left. "
					, Text "This creates a new team which you can edit."
					]
				, PTag []
					[ Text "If you have already created a team, you can find it in the Teams directory."
					]
				]) >>*
		[ OnAction ActionClose $ always $ return ()
		]

LINE_SEPARATOR :== IF_WINDOWS "\r\n" (IF_MAC "\r" "\n")

defaultProjectFile :: !String -> String
defaultProjectFile id = 'Text'.join LINE_SEPARATOR
	[ "Version: 1.5"
	, "Global"
	, "\tProjectRoot:\t."
	, "\tTarget:\tStdEnv"
	, "\tExec:\t{Project}*Team_"+++id+++".exe"
	, "\tByteCode:\t{Project}*Team_"+++id+++".bc"
	, "\tCodeGen"
	, "\t\tCheckStacks:\tFalse"
	, "\t\tCheckIndexes:\tTrue"
	, "\t\tOptimiseABC:\tTrue"
	, "\t\tGenerateByteCode:\tTrue"
	, "\tApplication"
	, "\t\tHeapSize:\t134217728"
	, "\t\tStackSize:\t1048576"
	, "\t\tExtraMemory:\t81920"
	, "\t\tIntialHeapSize:\t204800"
	, "\t\tHeapSizeMultiplier:\t4096"
	, "\t\tShowExecutionTime:\tFalse"
	, "\t\tShowGC:\tFalse"
	, "\t\tShowStackSize:\tFalse"
	, "\t\tMarkingCollector:\tFalse"
	, "\t\tDisableRTSFlags:\tFalse"
	, "\t\tStandardRuntimeEnv:\tTrue"
	, "\t\tProfile"
	, "\t\t\tMemory:\tFalse"
	, "\t\t\tMemoryMinimumHeapSize:\t0"
	, "\t\t\tTime:\tFalse"
	, "\t\t\tCallgraph:\tFalse"
	, "\t\t\tStack:\tFalse"
	, "\t\t\tDynamics:\tFalse"
	, "\t\t\tGenericFusion:\tFalse"
	, "\t\t\tDescExL:\tFalse"
	, "\t\tOutput"
	, "\t\t\tOutput:\tShowConstructors"
	, "\t\t\tFont:\tMonaco"
	, "\t\t\tFontSize:\t9"
	, "\t\t\tWriteStdErr:\tFalse"
	, "\tLink"
	, "\t\tLinkMethod:\tStatic"
	, "\t\tGenerateRelocations:\tFalse"
	, "\t\tGenerateSymbolTable:\tFalse"
	, "\t\tGenerateLinkMap:\tFalse"
	, "\t\tLinkResources:\tFalse"
	, "\t\tResourceSource:\t"
	, "\t\tGenerateDLL:\tFalse"
	, "\t\tExportedNames:\t"
	, "\t\tStripByteCode:\tTrue"
	, "\t\tKeepByteCodeSymbols:\tTrue"
	, "\t\tPrelinkByteCode:\tFalse"
	, "\tPaths"
	, "\t\tPath:\t{Project}"
	, "\t\tPath:\t{Project}*..*Game"
	, "\t\tPath:\t{Project}*..*StdLibExt"
	, "\t\tPath:\t{Project}*..*StdReferee"
	, "\t\tPath:\t{Project}*..*StdTeam"
	, "\tPrecompile:\t"
	, "\tPostlink:\t"
	, "MainModule"
	, "\tName:\tTeam_"+++id
	, "\tDir:\t{Project}"
	, "\tCompiler"
	, "\t\tNeverMemoryProfile:\tFalse"
	, "\t\tNeverTimeProfile:\tFalse"
	, "\t\tStrictnessAnalysis:\tTrue"
	, "\t\tListTypes:\tStrictExportTypes"
	, "\t\tListAttributes:\tTrue"
	, "\t\tWarnings:\tTrue"
	, "\t\tVerbose:\tTrue"
	, "\t\tReadableABC:\tFalse"
	, "\t\tReuseUniqueNodes:\tTrue"
	, "\t\tFusion:\tFalse"
	]

defaultTeamImplementation :: !String !String -> String
defaultTeamImplementation id name = 'Text'.join LINE_SEPARATOR
	[ "module Team_"+++id
	, ""
	, "import StdEnv"
	, "import Footballer"
	, "import FootballerFunctions"
	, ""
	, "Start :: TeamDefinition"
	, "Start ="
	, "\t{ teamId       = \""+++id+++"\""
	, "\t, teamFunction = wrapTeamFunction initialSetup"
	, "\t}"
	, ""
	, "initialSetup :: !Home !FootballField -> [Footballer]"
	, "initialSetup home field"
	, "| home=:West = westTeam"
	, "| otherwise  = eastTeam"
	, "where"
	, "\teastTeam = mirror field westTeam"
	, "\twestTeam = [keeper:fielders]"
	, "\tclubname = \""+++name+++"\" +++ if (home=:West) \"W\" \"E\""
	, "\tkeeper   = player clubname home field {zero & px=scale -0.5 field.flength} 1"
	, "\tfielders ="
	, "\t\t[  player clubname home field {px=scale (-0.5*dx) field.flength,py=scale (0.5*dy) field.fwidth} nr"
	, "\t\t\\\\ (dx,dy) <- west_positions_fielders"
	, "\t\t & nr      <- [2..]"
	, "\t\t]"
	, "\twest_positions_fielders ="
	, "\t\t[ (0.20, 0.40)"
	, "\t\t, (0.20,-0.40)"
	, "\t\t, (0.23, 0.00)"
	, "\t\t, (0.50, 0.45)"
	, "\t\t, (0.50,-0.45)"
	, "\t\t, (0.60, 0.00)"
	, "\t\t, (0.70, 0.35)"
	, "\t\t, (0.70,-0.35)"
	, "\t\t, (0.90, 0.05)"
	, "\t\t, (0.90,-0.05)"
	, "\t\t]"
	, ""
	, ":: Memory ="
	, "\t{ home :: !Home"
	, "\t}"
	, ""
	, "player :: !ClubName !Home !FootballField !Position !PlayersNumber -> Footballer"
	, "player club home field position nr ="
	, "\t{ playerID = {clubName=club,playerNr=nr}"
	, "\t, name     = toString nr"
	, "\t, length   = min_length"
	, "\t, pos      = position"
	, "\t, nose     = zero"
	, "\t, speed    = zero"
	, "\t, skills   = (Running, Kicking, Rotating)"
	, "\t, effect   = ?None"
	, "\t, stamina  = max_stamina"
	, "\t, health   = max_health"
	, "\t, brain    = {memory={home=home}, ai=wrapFootballerAI (brain field)}"
	, "\t}"
	, ""
	, "brain :: !FootballField !(!BrainInput, !Memory) -> (!BrainOutput,!Memory)"
	, "brain field (input=:{me,referee}, memory=:{home})"
	, "| d <= maxKickReach me = kick (centerOfGoal (other home) field) (input, new_memory)"
	, "| otherwise            = track_ball zero (input, new_memory)"
	, "where"
	, "\td          = dist me (getBall input)"
	, "\tnew_memory = if (any isEndHalf referee) {memory & home=other home} memory"
	]
