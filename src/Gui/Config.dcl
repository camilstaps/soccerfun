definition module Gui.Config

from Data.GenEq import generic gEq
from System.FilePath import :: FilePath
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.SDS.Definition import :: SDSLens, :: SimpleSDSLens
from iTasks.UI.Editor import :: Editor
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import :: Task, class iTask

from matchGame import :: Options

:: Configuration =
	{ options                      :: !Options
	, clean_installation_directory :: !FilePath
	, team_heap_size               :: !Int
	, team_stack_size              :: !Int
	}

derive class iTask Configuration

configurationFile :: String

theConfiguration :: SimpleSDSLens Configuration

editConfiguration :: Task ()
