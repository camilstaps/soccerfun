const path=require ('path');
const process=require ('process');

if (__dirname.substr (__dirname.length-14).match (/[\/\\]resources[\/\\]app/))
	process.chdir (path.join (__dirname,'..','..'));
else
	process.chdir (__dirname);

const {run}=require ('./SoccerFun-www/js/clean-electron.js');

const opts={
	app: 'SoccerFun'
};

run (opts);
