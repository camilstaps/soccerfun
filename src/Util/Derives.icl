implementation module Util.Derives

from Data.Func import $
import Data.Functor
import Data.Maybe
import Text.GenJSON

import iTasks.Internal.Generic.Visualization => qualified <+++, +++>
import iTasks.Internal.Serialization // for (->)
import iTasks.UI.Editor.Controls
import iTasks.UI.Editor.Generic
import iTasks.UI.Editor.Modifiers => qualified <+++, +++>

import Footballer
import Geometry
import RandomExt
import Team
import matchControl
import matchGame

derive class iTask
	Edge, Fallible, FootballerID, Home, FootballField, Options, Position,
	RefereeAction, Reprimand

gEq{|Footballer|} a b = a == b
gEq{|Metre|} a b = a == b
gEq{|Minutes|} a b = a == b
gEq{|PlayingTime|} a b = a == b
gEq{|Referee|} a b = nameOf a == nameOf b
gEq{|RefereeDefinition|} (RefereeDefinition a) (RefereeDefinition b) =
	nameOf (a getDefaultField) == nameOf (b getDefaultField)
gEq{|Seconds|} a b = a == b
gEq{|TeamDefinition|} a b = a.teamId == b.teamId

derive gEq
	Angle, Football, FootballGame, FootballState, Half, Match, Position3D,
	RandomSeed, Speed, Speed3D, Velocity

derive JSONEncode
	FeintDirection, Football, FootballGame, FootballState,
	FootballerAction, Half, Match, PlayingTime, Position3D, RefereeDefinition,
	Skill, Speed, Speed3D, TeamDefinition
derive JSONDecode
	FeintDirection, Football, FootballGame, FootballState,
	FootballerAction, Half, Match, PlayingTime, Position3D, RefereeDefinition,
	Skill, Speed, Speed3D, TeamDefinition

JSONEncode{|Angle|} b a = JSONEncode{|*|} b (toReal a)
JSONEncode{|Metre|} b m = JSONEncode{|*|} b (toReal m)
JSONEncode{|Minutes|} b m = JSONEncode{|*|} b (toReal m)
JSONEncode{|RandomSeed|} b s = JSONEncode{|*|} b (toInt (toString s))
JSONEncode{|Seconds|} b s = JSONEncode{|*|} b (toReal s)
JSONEncode{|Velocity|} b v = JSONEncode{|*|} b (toReal v)

JSONDecode{|Angle|} b json = case JSONDecode{|*|} b json of
	(?Just r,json) -> (?Just (fromReal r), json)
	(?None,json) -> (?None, json)
JSONDecode{|Metre|} b json = case JSONDecode{|*|} b json of
	(?Just r,json) -> (?Just (fromReal r), json)
	(?None,json) -> (?None, json)
JSONDecode{|Minutes|} b json = case JSONDecode{|*|} b json of
	(?Just r,json) -> (?Just (fromReal r), json)
	(?None,json) -> (?None, json)
JSONDecode{|RandomSeed|} b json = case JSONDecode{|*|} b json of
	(?Just r,json) -> (?Just (fromString r), json)
	(?None,json) -> (?None, json)
JSONDecode{|Seconds|} b json = case JSONDecode{|*|} b json of
	(?Just r,json) -> (?Just (fromReal r), json)
	(?None,json) -> (?None, json)
JSONDecode{|Velocity|} b json = case JSONDecode{|*|} b json of
	(?Just r,json) -> (?Just (fromReal r), json)
	(?None,json) -> (?None, json)

JSONEncode{|Footballer|} _ _ = abort "JSONEncode{|Footballer|}\n"
JSONDecode{|Footballer|} _ _ = abort "JSONDecode{|Footballer|}\n"
JSONEncode{|Referee|} _ _ = abort "JSONEncode{|Referee|}\n"
JSONDecode{|Referee|} _ _ = abort "JSONDecode{|Referee|}\n"

gText{|Footballer|} _ _ = []

gText{|Metre|} _ (?Just m) = [toString m]
gText{|Metre|} _ ?None     = []

gText{|Minutes|} _ (?Just m) = [toString m]
gText{|Minutes|} _ ?None     = []

gText{|PlayingTime|} f (?Just (PlayingTime t)) = gText{|*|} f (?Just t)
gText{|PlayingTime|} _ ?None                   = []

gText{|Referee|} _ (?Just r) = [nameOf r]
gText{|Referee|} _ ?None     = []

gText{|RefereeDefinition|} _ (?Just (RefereeDefinition r)) = [nameOf (r getDefaultField)]
gText{|RefereeDefinition|} _ ?None                         = []

gText{|TeamDefinition|} _ (?Just a) = [a.teamId]
gText{|TeamDefinition|} _ ?None     = []

gText{|FootballGame|} _ _ = abort "gText{|FootballGame|}\n"

gEditor{|Footballer|} _ = abort "gEditor{|Footballer|}\n"

gEditor{|Metre|} purpose =
	mapEditorRead toReal $
	mapEditorWrite (fmap fromReal) $
	gEditor{|*|} purpose
gEditor{|Minutes|} purpose =
	mapEditorRead (toInt o toReal) $
	mapEditorWrite (fmap (minutes o toReal)) $
	gEditor{|*|} purpose

gEditor{|PlayingTime|} purpose =
	mapEditorRead (\(PlayingTime t) -> toInt (toReal t)) $
	mapEditorWrite (fmap \i -> PlayingTime (minutes (toReal i)))
	(gEditor{|*|} purpose)

gEditor{|Referee|} _ = abort "gEditor{|Referee|}\n"
gEditor{|RefereeDefinition|} _ = abort "gEditor{|RefereeDefinition|}\n"

gEditor{|TeamDefinition|} _ = abort "gEditor{|TeamDefinition|}\n"

gEditor{|FootballGame|} _ = abort "gEditor{|FootballGame|}\n"
