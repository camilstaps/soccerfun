definition module Util.Derives

from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import class iTask

from Football import
	:: FootballState, :: Football
from Footballer import
	:: Edge, :: FeintDirection, :: Footballer, :: FootballField,
	:: FootballerAction, :: FootballerID, :: Home, :: Minutes,
	:: RefereeAction, :: Reprimand, :: Skill
from Geometry import
	:: Angle, :: Metre, :: Position, :: Position3D, :: Speed, :: Speed3D,
	:: Velocity
from RandomExt import
	:: RandomSeed
from Referee import
	:: Referee, :: RefereeDefinition
from StdEnvExt import
	:: Fallible
from Team import
	:: TeamDefinition
from matchControl import
	:: PlayingTime, :: Seconds
from matchGame import
	:: FootballGame, :: Options

derive class iTask
	Edge, Fallible,
	Home, FootballField, FootballerID, RefereeAction, Reprimand,
	Position,
	Options

derive gEq
	Footballer, Metre, Minutes, PlayingTime, Referee, RefereeDefinition,
	TeamDefinition,
	FootballGame

derive JSONEncode
	FootballState, Football,
	FeintDirection, Footballer, FootballerAction, Minutes, Skill, Angle, Metre,
	Position3D, Speed, Speed3D, Velocity,
	RandomSeed,
	Referee, RefereeDefinition,
	TeamDefinition,
	PlayingTime, Seconds,
	FootballGame
derive JSONDecode
	FootballState, Football,
	FeintDirection, Footballer, FootballerAction, Minutes, Skill, Angle, Metre,
	Position3D, Speed, Speed3D, Velocity,
	RandomSeed,
	Referee, RefereeDefinition,
	TeamDefinition,
	PlayingTime, Seconds,
	FootballGame

derive gText
	Footballer, Metre, Minutes, PlayingTime, Referee, RefereeDefinition,
	TeamDefinition,
	FootballGame

derive gEditor
	Footballer, Metre, Minutes, PlayingTime, Referee, RefereeDefinition,
	TeamDefinition,
	FootballGame
