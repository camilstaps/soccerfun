definition module matchLog

/**	This module defines the logging facilities of Soccer-Fun.
*/
import matchControl

logFile			:: String

/*	logMatchStart match env
		writes the static game information to logFile.
*/
logMatchStart	:: !Match !*env -> *env | FileSystem env

/*	logMatch match refereeActions succeededActions env
		writes the game state to logFile.
*/
logMatch		:: !Match ![RefereeAction] !(AssocList FootballerID FootballerAction) !*env -> *env | FileSystem env

/*	logMatchStart match env
		writes the match result to logFile.
*/
logMatchEnd		:: !Match !*env -> *env | FileSystem env
