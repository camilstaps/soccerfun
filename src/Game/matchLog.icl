implementation module matchLog

import StdEnvExt, fileIO
import Text.GenJSON
import matchControl
import Util.Derives

logFile						:: String
logFile						= "SoccerFun.jsonl"

:: LogStartEntry =
	{ team1				:: ![LogFootballer]
	, team2				:: ![LogFootballer]
	, playingTime		:: !Minutes
	, unittime			:: !TimeUnit
	, field				:: !FootballField
	, seed				:: !RandomSeed
	}

:: LogEntry =
	{ /*actions			:: ![LogEntryFootballerAction]
	,*/ positions			:: ![LogEntryFootballerPosition]
	, referee			:: ![RefereeAction]
	, ball_position		:: !Position3D
	, time				:: !Minutes
	}

:: LogEndEntry =
	{ score				:: !Score
	}

:: LogEntryFootballerAction =
	{ player			:: !FootballerID
	, action			:: !FootballerAction
	}

:: LogEntryFootballerPosition =
	{ player			:: !FootballerID
	, position			:: !Position
	}

:: LogFootballer =
	{ playerID	:: !FootballerID
	, name		:: !String
	, length	:: !Length
	, pos		:: !Position
	, speed		:: !Speed
	, nose		:: !Angle
	, skills	:: !MajorSkills
	, stamina	:: !Stamina
	, health	:: !Health
	}

derive JSONEncode LogStartEntry, LogEntry, LogEndEntry,
	LogEntryFootballerAction, LogEntryFootballerPosition, LogFootballer

log :: !e !*env -> *env | FileSystem env & JSONEncode{|*|} e
log entry env
	# (ok,f,env)		= fopen logFile FAppendText env
	| not ok			= abort ("Failed to open "+++logFile+++".\n")
	# f					= f <<< toJSON entry <<< "\n"
	# (ok,env)			= fclose f env
	| not ok			= abort ("Failed to close "+++logFile+++".\n")
	| otherwise			= env

logMatchStart :: !Match !*env -> *env | FileSystem env
logMatchStart {team1,team2,playingTime,unittime,theField,seed} env
	# (ok,f,env)		= fopen logFile FWriteText env
	| not ok			= abort ("Failed to open "+++logFile+++".\n")
	# f					= f <<< ""
	# (ok,env)			= fclose f env
	| not ok			= abort ("Failed to close "+++logFile+++".\n")
	| otherwise			= log entry env
where
	entry =
		{ team1			= map toLogFootballer team1
		, team2			= map toLogFootballer team2
		, playingTime	= playingTime
		, unittime		= unittime
		, field			= theField
		, seed			= seed
		}

	toLogFootballer footballer =
		{ LogFootballer
		| playerID		= footballer.Footballer.playerID
		, name			= footballer.Footballer.name
		, length		= footballer.Footballer.length
		, pos			= footballer.Footballer.pos
		, speed			= footballer.Footballer.speed
		, nose			= footballer.Footballer.nose
		, skills		= footballer.Footballer.skills
		, stamina		= footballer.Footballer.stamina
		, health		= footballer.Footballer.health
		}

logMatch :: !Match ![RefereeAction] !(AssocList FootballerID FootballerAction) !*env -> *env | FileSystem env
logMatch {Match | team1,team2,playingTime,theBall} refereeActions succeededActions env = log entry env
where
	allPlayers			= team1 ++ team2
	ball				= getFootball theBall allPlayers

	entry =
		{ /*actions		= [{player=id, action=action} \\ (id,action) <- succeededActions]
		,*/ positions		= [{player=playerID, position=pos} \\ {Footballer | playerID,pos} <- allPlayers]
		, referee		= refereeActions
		, ball_position	= ball.ballPos
		, time			= playingTime
		}

logMatchEnd :: !Match !*env -> *env | FileSystem env
logMatchEnd {Match | score} env = log entry env
where
	entry =
		{ LogEndEntry
		| score			= score
		}
