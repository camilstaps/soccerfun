definition module matchGame

/** This module defines the match and tournament data structures.
*/
import matchControl
import RandomExt

::	FootballGame
	=	{ team1				:: !TeamDefinition				// the first team
		, team2				:: !TeamDefinition				// the second team
		, referee			:: !RefereeDefinition			// the referee definition
		, match				:: !Match						// the football match to be played
		, gameOver			:: !Bool						// whether the game has finished
		, frames			:: !Int							// nr of frames so far (reset to zero every second)
		, options			:: !Options						// options of football game
		}

::	Options
	=	{ playingTime		:: !PlayingTime					// default playingtime (defaultPlayingTime)
		, useRandomSeed		:: !Bool						// set to False for realistic behaviour
		, logging			:: !Bool						// write events to SoccerFun.jsonl
		}

/**	incFrames game increases the frames count of game.
*/
incFrames					:: !FootballGame -> FootballGame

/*	defaultPlayingTime returns recommended playing time
*/
defaultPlayingTime			:: PlayingTime

/**	defaultOptions returns default options.
*/
defaultOptions				:: Options

/*	timeLeft is True if the game has not finished
*/
timeLeft					:: !FootballGame -> Bool

::	Competition				=	{ results		:: ![[?Score]]		// teams x teams matrix of match results (note: team x team -> ?None)
								, west			:: ![ClubName]		// names of participating teams (west side)
								, east			:: ![ClubName]		// names of participating teams (east side)
								, usedRandomSeed:: !RandomSeed		// the seed that is used for computing the matches
								}
::	Ranking					:== AssocList ClubName Rank
::	Rank					=	{ matchpoints	:: !Int				// number of matchpoints   (>= 0)
								, goals_scored	:: !Int				// number of scored goals  (>= 0)
								, goals_against	:: !Int				// number of goals against (>= 0)
								}
instance zero Rank
instance ==   Rank
instance <    Rank
instance +    Rank

/** competition teams field referee time rs
		computes an entire competition between all teams in teams.
		Each match uses the same referee and same initial random seed value rs.
*/
competition					:: ![Home FootballField -> Team] !FootballField !Referee !PlayingTime !RandomSeed -> Competition

/** computeMatch match
		computes an entire match between the currently selected team1 and team2.
*/
computeMatch				:: !Match -> Score

/** ranking competition
		computes the ranking of all teams that have participated in competition.
*/
//ranking						:: !Competition -> Ranking
ranking						:: ![ClubName] ![?Score] -> Ranking

/** checkCompetitionFile west_team_names rs env
		checks whether there is a competition backup file present for the current set
		of teams (assuming they start on the West home side) and initial random seed value rs
		for computing matches. 
		If not, then such a file is created, and the same random seed value and empty list of scores is returned. 
		If so, then the currently stored random seed value and list of scores is returned.
*/
checkCompetitionFile		:: ![ClubName] !RandomSeed !*env -> (!(!RandomSeed,![?Score]),!*env) | FileSystem env

/** appendMatchToCompetitionFile west east env
		appends an empty entry of a match between west versus east in the competition backup file.
		It also returns the file pointer to allow a correct update in updateMatchToCompetitionFile.
*/
appendMatchToCompetitionFile:: !ClubName !ClubName !*env -> (!Int,!*env) | FileSystem env


/** updateMatchToCompetitionFile west east score filepointer env
		updates the line that starts at filepointer in the competition backup file with the result 
		of the match between west versus east.
*/
updateMatchToCompetitionFile:: !ClubName !ClubName !(?Score) !Int !*env -> *env | FileSystem env
