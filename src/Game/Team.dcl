definition module Team

/** This module defines the Soccer-Fun API that is concerned with teams.
	All available teams are collected in this module (allAvailableTeams).
*/
import Footballer

allAvailableTeams	:: [TeamDefinition]

::	Team			:== [Footballer]		// the fielders are supposed to have different numbers, and all not equal to 1

::	TeamDefinition	=	{ teamId		:: !String
						, teamFunction	:: !Home FootballField -> Fallible Team
						}

instance nameOf   	Team

wrapTeamFunction	:: !(Home FootballField -> Team) -> Home -> FootballField -> Fallible Team
validateTeam		:: !Team -> Team
isValidTeam			:: !Team -> Bool
allPlayersAtHome	:: !FootballField !Home !Team -> Bool
replaceInTeam		:: ![Footballer] !Team -> Team
getTeam				:: !ClubName ![Team] -> Team

class mirror a		:: !FootballField !a -> a
instance mirror [a] | mirror a
instance mirror Footballer
instance mirror Position
instance mirror Speed
instance mirror Angle
