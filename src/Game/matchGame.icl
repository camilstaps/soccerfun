implementation module matchGame

import StdEnvExt, fileIO
import matchControl
import Text.GenJSON
import Util.Derives
	
timeLeft					:: !FootballGame -> Bool
timeLeft game				= game.match.Match.playingTime > zero

defaultPlayingTime			:: PlayingTime
defaultPlayingTime			= PlayingTime (minutes 1.0)

incFrames					:: !FootballGame -> FootballGame
incFrames game=:{frames}	= {game & frames=frames+1}

instance zero Rank where
	zero					= { matchpoints = zero, goals_scored = zero, goals_against = zero }
instance == Rank where
	(==) r1 r2				= (r1.matchpoints,r1.goals_scored,r1.goals_against) == (r2.matchpoints,r2.goals_scored,r2.goals_against)
instance < Rank where
	(<) r1 r2				= r1.matchpoints <  r2.matchpoints || 
							  r1.matchpoints == r2.matchpoints && r1.goals_scored <  r2.goals_scored ||
							  r1.matchpoints == r2.matchpoints && r1.goals_scored == r2.goals_scored && r1.goals_against > r2.goals_against
instance + Rank where
	(+) r1 r2				= { matchpoints   = r1.matchpoints   + r2.matchpoints
							  , goals_scored  = r1.goals_scored  + r2.goals_scored
							  , goals_against = r1.goals_against + r2.goals_against
							  }

competition					:: ![Home FootballField -> Team] !FootballField !Referee !PlayingTime !RandomSeed -> Competition
competition teams field referee playingtime rs
	= { results				= [ [  if (nr_west == nr_east) 
							          ?None
							          (?Just (computeMatch (setMatchStart (team_west West field) (team_east East field) field referee playingtime rs)))
							    \\ (nr_east,team_east) <- zip2 [1..] teams
							    ]
							  \\ (nr_west,team_west) <- zip2 [1..] teams
							  ]
	  , west				= map (\f -> nameOf (f West field)) teams
	  , east				= map (\f -> nameOf (f East field)) teams
	  , usedRandomSeed		= rs
	  }

computeMatch				:: !Match -> Score
computeMatch match
| match.Match.playingTime > zero
							= computeMatch (snd (stepMatch match))
| otherwise					= match.score

ranking						:: ![ClubName] ![?Score] -> Ranking
ranking names scores		= foldl upd [(t,zero) \\ t <- names] (zip2 [(tw,te) \\ tw <- names, te <- names] scores)
where	
	upd ranking (_,?None)
		= ranking
	upd ranking ((west,east),?Just (goals_west,goals_east))
		= updkeyvalue west ((+) rank_west) (updkeyvalue east ((+) rank_east) ranking)
	where
		(mps_west, mps_east)	= if (goals_west > goals_east) (3,0) (if (goals_west < goals_east) (0,3) (1,1))
		(rank_west,rank_east)	= ({matchpoints=mps_west,goals_scored=goals_west,goals_against=goals_east}
								  ,{matchpoints=mps_east,goals_scored=goals_east,goals_against=goals_west}
								  )

defaultOptions				:: Options
defaultOptions
	= { playingTime			= defaultPlayingTime
	  , useRandomSeed		= True
	  , logging				= False
	  }

checkCompetitionFile		:: ![ClubName] !RandomSeed !*env -> (!(!RandomSeed,![?Score]),!*env) | FileSystem env
checkCompetitionFile west rs env
# (ok,cf,env)				= fopen competitionFile FReadText env
| not ok					= ((rs,[]), createCompetitionFile west rs env)						// competition file does not exist: create it
# (ok,frs,fwest,cf)			= header cf
| not ok || fwest <> teams_line west
							= ((rs,[]), createCompetitionFile west rs (snd (fclose cf env)))	// competition file ill-formatted or different set of teams: create it
# (scores,cf)				= readScores cf														// competition file exists, and for this competition
# (ok,env)					= fclose cf env
| not ok					= abort ("Could not close competition file after reading scores.\n" <+++ length scores)
| otherwise					= ((frs,scores),env)
where
	readScores				:: !*File -> (![?Score],!*File)
	readScores cf
	# (end,cf)				= fend cf
	| end					= ([],cf)
	# (line,cf)				= freadline cf
	# score					= if (line.[0] == 'x') ?None
							 (let (i1,l1)	= span ((<>) ' ') [c \\ c<-:line]
							      (i2,l2)	= span ((<>) ' ') (tl l1)
							   in ?Just (toInt (toString i1),toInt (toString i2))
							 )
	# (scores,cf)			= readScores cf
	= ([score:scores],cf)

appendMatchToCompetitionFile:: !ClubName !ClubName !*env -> (!Int,!*env) | FileSystem env
appendMatchToCompetitionFile west east env
# (ok,cf,env)				= fopen competitionFile FAppendText env
| not ok					= abort "Could not open competition file for appending data.\n"
# (pos,cf)					= fposition cf
# (ok,env)					= fclose (cf <<< "x " <<< west <<< " vs " <<< east <<< '\n') env
| not ok					= abort "Could not close competition file after appending data.\n"
| otherwise					= (pos,env)

updateMatchToCompetitionFile:: !ClubName !ClubName !(?Score) !Int !*env -> *env | FileSystem env
updateMatchToCompetitionFile west east score pos env
# (ok,cf,env)				= fopen competitionFile FAppendText env
| not ok					= abort "Could not open competition file for appending data.\n"
# (ok,cf)					= fseek cf pos FSeekSet
| not ok					= abort "Could not seek in competition file for updating data.\n"
# (ok,env)					= fclose (cf <<< result <<< ' ' <<< west <<< " vs " <<< east <<< '\n') env
| not ok					= abort "Could not close competition file after appending data.\n"
| otherwise					= env
where
	result					= case score of
								?None         = "x"
								?Just (gw,ge) = gw +++> (" " <+++ ge)

createCompetitionFile		:: ![ClubName] !RandomSeed !*env -> *env | FileSystem env
createCompetitionFile west rs env
# (ok,cf,env)				= fopen competitionFile FWriteText env
| not ok					= abort "Could not create competition file.\n"
# (ok,env)					= fclose (cf <<< seed_line rs <<< '\n' <<< teams_line west <<< '\n') env
| not ok					= abort "Could not close competition file.\n"
| otherwise					= env

header						:: !*File -> (!Bool,!RandomSeed,!String,!*File)
header file
# (rs_line,   file)			= freadline file
# (teams_line,file)			= freadline file
= (size rs_line > 1 && size teams_line > 1, fromString (rs_line%(0,size rs_line-2)), teams_line%(0,size teams_line-2),file)

seed_line					:: !RandomSeed -> String
seed_line rs				= toString rs

teams_line					:: ![ClubName] -> String
teams_line west				= foldl (\t ts -> t +++ "," +++ ts) "" west

competitionFile				:== "competition.txt"
