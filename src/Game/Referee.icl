implementation module Referee

import StdEnvExt
import matchGame
import Umpire
import NoReferee
//	When coding for all referees, include following modules:
import RefereeCoach_Rounds_Assignment
import RefereeCoach_Slalom_Assignment
import RefereeCoach_Passing_Assignment
import RefereeCoach_DeepPass_Assignment
import RefereeCoach_Keeper_Assignment

allAvailableReferees	:: [RefereeDefinition]
allAvailableReferees	= [ RefereeDefinition umpire
						  , RefereeDefinition NoReferee
						  ]
//	When coding for all referees, use following list:
							++
						  [ RefereeDefinition RefereeCoach_Rounds
						  , RefereeDefinition RefereeCoach_Slalom
						  , RefereeDefinition RefereeCoach_Passing
						  , RefereeDefinition RefereeCoach_DeepPass
						  , RefereeDefinition RefereeCoach_Keeper
						  ]

instance nameOf Referee where nameOf {Referee | name} = name

defaultReferee			:: Referee
defaultReferee			= { name			= "Default"
						  , brain			= {memory = (),ai = \(_,st) -> ([ContinueGame],st)}
						  , refActionPics	= []
						  }

initialReferee			:: !RefereeDefinition !FootballField -> Referee
initialReferee (RefereeDefinition r) field
						= r field

randomlessRefereeAI		:: (RefereeAI msg memory) -> RefereeAI msg (memory,RandomSeed)
randomlessRefereeAI f	= \(input,(memory,seed))	= let (decisions,memory`) = f (input,memory) in (decisions,(memory`,seed))

amnesiaRefereeAI		:: (RefereeAI msg RandomSeed) -> RefereeAI msg (memory,RandomSeed)
amnesiaRefereeAI f		= \(input,(memory,seed))	= let (decisions,seed`) = f (input,seed) in (decisions,(memory,seed`))

witlessRefereeAI		:: (RefereeAI` msg) -> RefereeAI msg (memory,RandomSeed)
witlessRefereeAI f		= \(input,state)			= (f input,state)
