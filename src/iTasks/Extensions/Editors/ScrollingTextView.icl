implementation module iTasks.Extensions.Editors.ScrollingTextView

import StdEnv

import iTasks

import ABC.Interpreter.JavaScript

scrollingTextView :: Editor String ()
scrollingTextView = withClientSideInit initUI textView

initUI :: FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
initUI _ me w
	# (scroll,w) = jsWrapFun (scroll me) me w
	# (orgInitDOMEl,w) = me .# "initDOMEl" .? w
	# (initDOMEl,w) = jsWrapFun (initDOMEl me orgInitDOMEl scroll) me w
	# w = (me .# "initDOMEl" .= initDOMEl) w
	# (orgOnAttributeChange,w) = me .# "onAttributeChange" .? w
	# (onAttributeChange,w) = jsWrapFun (onAttributeChange me orgOnAttributeChange scroll) me w
	# w = (me .# "onAttributeChange" .= onAttributeChange) w
	= w

// NB: setTimeout seems to be needed because otherwise the scrollHeight is not
// recalculated before calling scrollTo.
initDOMEl me orgInitDOMEl scroll _ w
	# w = (jsCall (orgInitDOMEl .# "bind") me .$! ()) w
	= (jsWindow .# "setTimeout" .$! (scroll, 1)) w
onAttributeChange me orgOnAttributeChange scroll {[0]=name,[1]=val} w
	# w = (jsCall (orgOnAttributeChange .# "bind") me .$! (name, val)) w
	= (jsWindow .# "setTimeout" .$! (scroll, 1)) w

scroll me _ w
	= (me .# "domEl.scrollTo" .$! (0, me .# "domEl.scrollHeight")) w
