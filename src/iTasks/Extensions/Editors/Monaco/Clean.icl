implementation module iTasks.Extensions.Editors.Monaco.Clean

import ABC.Interpreter.JavaScript

cleanMonarchDefinition :: JSRecord
cleanMonarchDefinition = jsRecord
	[ "keywords" :>
		[ "foreign", "export", "ccall", "stdcall"
		, "code", "inline"
		, "implementation", "definition", "system", "module"
		, "import", "qualified", "as", "from", "library"
		, "infixl", "infixr", "infix"
		, "if", "otherwise"
		, "let", "in", "case", "of", "with", "where"
		, "class", "instance", "special"
		, "generic", "derive"
		, "dynamic"
		]

	// C# style strings
	, "escapes" :> jsGlobal "/\\\\(?:[abfnrtv\\\\\"']|x[0-9A-Fa-f]{1,2}|0\\d{0,2})/"

	, "brackets" :>
		[ ("{", "}", "delimiter.curly")
		, ("[", "]", "delimiter.square")
		, ("(", ")", "delimiter.parenthesis")
		]

	// The main tokenizer for our languages
	, "tokenizer" :> jsRecord
		[ "_root" :> // underscore is needed so that it is the first when alphabetized
			// identifiers and keywords
			[ toJS
				( jsGlobal "/[a-z_$][\\w$]*/"
				, jsRecord
					[ "cases" :> jsRecord
						[ "@keywords" :> "keyword"
						, "@default" :> "identifier"
						]
					]
				)
			, toJS
				( jsGlobal "/[A-Z][\\w\\$]*/"
				, jsRecord
					[ "cases" :> jsRecord
						[ "Start" :> "identifier"
						, "@default" :> "type.identifier"
						]
					]
				)

			// whitespace
			, toJS (jsRecord ["include" :> "@whitespace"])

			// list of characters shorthand (must be before brackets)
			, toJS (jsGlobal "/\\['/", jsRecord ["token" :> "string.quote", "bracket" :> "@open", "next" :> "@charlist"])

			// delimiters and operators
			, toJS (jsGlobal "/[{}()\\[\\]]/", "@brackets")
			, toJS (jsGlobal "/[;,.]/", "delimiter")

			// numbers
			, toJS (jsGlobal "/\\d+\\.\\d+([eE][\\-+]?\\d+)?/", "number.float")
			, toJS (jsGlobal "/0[xX][0-9a-fA-F]+/", "number.hex")
			, toJS (jsGlobal "/\d+/", "number")

			// strings
			, toJS (jsGlobal "/\"([^\"\\\\]|\\\\.)*$/", "string.invalid") // non-teminated string
			, toJS (jsGlobal "/\"/", jsRecord ["token" :> "string.quote", "bracket" :> "@open", "next" :> "@string"])

			// characters
			, toJS (jsGlobal "/'[^\\\\']'/", "string")
			, toJS (jsGlobal "/(')(@escapes)(')/", ["string","string.escape","string"])
			, toJS (jsGlobal "/'/", "string.invalid")
			]

		, "comment" :>
			[ toJS (jsGlobal "/[^\\/*]+/", "comment")
			, toJS (jsGlobal "/\\/\\/.*/", "comment")
			, toJS (jsGlobal "/\\/\\*/",   "comment", "@push")
			, toJS (         "\\*/",       "comment", "@pop")
			, toJS (jsGlobal "/[\\/*]/",   "comment")
			]

		, "string" :>
			[ toJS (jsGlobal "/[^\\\\\"]+/", "string")
			, toJS (jsGlobal "/@escapes/",   "string.escape")
			, toJS (jsGlobal "/\\\\./",      "string.escape.invalid")
			, toJS (jsGlobal "/\"/", jsRecord ["token" :> "string.quote", "bracket" :> "@close", "next" :> "@pop"])
			]

		, "charlist" :>
			[ toJS (jsGlobal "/[^\\\\']+/", "string")
			, toJS (jsGlobal "/@escapes/",  "string.escape")
			, toJS (jsGlobal "/\\\\./",     "string.escape.invalid")
			, toJS (jsGlobal "/']/", jsRecord ["token" :> "string.quote", "bracket" :> "@close", "next" :> "@pop"])
			]

		, "whitespace" :>
			[ toJS (jsGlobal "/[ \\t\\r\\n]+/", "white")
			, toJS (jsGlobal "/\\/\\*/",        "comment", "@comment")
			, toJS (jsGlobal "/\\/\\/.*$/",     "comment")
			]
		]
	]

cleanMonarchConfig :: JSRecord
cleanMonarchConfig = jsRecord
	[ "brackets" :>
		[ ("{", "}")
		, ("[", "]")
		, ("(", ")")
		]
	, "surroundingPairs" :>
		[ jsRecord ["open" :> "{", "close" :> "}"]
		, jsRecord ["open" :> "[", "close" :> "]"]
		, jsRecord ["open" :> "(", "close" :> ")"]
		, jsRecord ["open" :> "'", "close" :> "'"]
		, jsRecord ["open" :> "\"", "close" :> "\""]
		]
	, "autoClosingPairs" :>
		[ jsRecord ["open" :> "{", "close" :> "}"]
		, jsRecord ["open" :> "[", "close" :> "]"]
		, jsRecord ["open" :> "(", "close" :> ")"]
		, jsRecord ["open" :> "'", "close" :> "'"]
		, jsRecord ["open" :> "\"", "close" :> "\""]
		]
	]
