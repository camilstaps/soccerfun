definition module iTasks.Extensions.Editors.Monaco

from StdOverloaded import class ==

from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import class iTask

:: MonacoValue =
	{ has_unsaved_changes :: !Bool
	, save_requested      :: !Bool
	, compile_requested   :: !Bool
	, contents            :: !String // This field is at the end for an efficient `gEq`
	}

derive class iTask \ gEditor MonacoValue
derive gEditor MonacoValue

instance == MonacoValue
