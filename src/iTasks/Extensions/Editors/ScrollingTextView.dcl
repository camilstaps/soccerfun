definition module iTasks.Extensions.Editors.ScrollingTextView

from iTasks.UI.Editor import :: Editor

//* A `textView` that automatically scrolls to the bottom when the value is updated.
scrollingTextView :: Editor String ()
