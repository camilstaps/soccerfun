implementation module iTasks.Extensions.Editors.Monaco

import StdEnv

import Data.Func
import qualified Data.Map

import iTasks
import iTasks.Extensions.Editors.Monaco.Clean

import ABC.Interpreter.JavaScript

derive class iTask \ gEditor MonacoValue

instance == MonacoValue
where
	(==) x y = gEq{|*|} x y

gEditor{|MonacoValue|} ViewValue = abort "gEditor{|MonacoValue|}: ViewValue\n"
gEditor{|MonacoValue|} EditValue = withClientSideInit initUI $ leafEditorToEditor
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = writeValue
	}

:: EditEvent
	= HasChanges
	| SetContent !String
	| RequestSave
	| RequestCompile

derive JSONDecode EditEvent
derive JSONEncode EditEvent

onReset :: !UIAttributes !(?MonacoValue) !*VSt -> (!*MaybeErrorString *(UI, MonacoValue, * ?(?MonacoValue)), !*VSt)
onReset attr (?Just val) vst =
	( Ok
		( uia UIHtmlView $ 'Data.Map'.union attr $ 'Data.Map'.singleton "contents" (JSONString val.contents)
		, val
		, ?None
		)
	, vst
	)

onEdit :: !EditEvent !MonacoValue !*VSt -> (!*MaybeErrorString *(UIChange, MonacoValue, * ?(?MonacoValue)), !*VSt)
onEdit HasChanges st vst
	# st & has_unsaved_changes = True
	= (Ok (NoChange, st, ?Just (?Just st)), vst)
onEdit (SetContent s) st vst
	# st & contents = s, save_requested = True
	= (Ok (NoChange, st, ?Just (?Just st)), vst)
onEdit RequestSave st vst
	# st & save_requested = True
	= (Ok (NoChange, st, ?Just (?Just st)), vst)
onEdit RequestCompile st vst
	# st & compile_requested = True
	= (Ok (NoChange, st, ?Just (?Just st)), vst)

onRefresh :: !MonacoValue !MonacoValue !*VSt -> (!*MaybeErrorString *(UIChange, MonacoValue, * ?(?MonacoValue)), !*VSt)
onRefresh new st vst =
	( Ok
		( if eq NoChange (ChangeUI [SetAttribute "contents" (JSONString new.contents)] [])
		, new
		, ?None
		)
	, vst
	)
where
	eq = new.contents == st.contents

writeValue :: !MonacoValue -> MaybeErrorString (?MonacoValue)
writeValue val = Ok (?Just val)

monaco :== jsGlobal "monaco"

initUI :: FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
initUI {FrontendEngineOptions | serverDirectory} me w
	# (initDOMEl,w) = jsWrapFun (initDOMEl me) me w
	# w = (me .# "initDOMEl" .= initDOMEl) w
	= w
initDOMEl me _ w
	// TODO: ideally use a require procedure that would also work in the browser
	# (electron,w) = (jsGlobal "require" .$ "electron") w
	  (isPackaged,w) = electron .# "remote.app.isPackaged" .?? (False, w)

	# (path,w) = (jsGlobal "require" .$ "path") w
	# (baseUrl,w) = (path .# "resolve" .$? if isPackaged "resources/app/" "" +++ "node_modules/monaco-editor/min") ("", w)
	  baseUrl = {if (c=='\\') '/' c \\ c <-: baseUrl}
	  (baseUrl,w) = (jsGlobal "encodeURI" .$ ("file:///"+++baseUrl)) w

	# (loader,w) = (jsGlobal "require" .$ "monaco-editor/min/vs/loader.js") w
	# (next,w) = jsWrapFun (initDOMEl` me) me w
	# w = (loader .# "require.config" .$! jsRecord ["baseUrl" :> baseUrl]) w
	# w = (jsGlobal "self.module" .= jsGlobal "undefined") w
	= (loader .# "require" .$! (["vs/editor/editor.main"], next)) w
initDOMEl` me _ w
	# w = (monaco .# "languages.register" .$! jsRecord ["id" :> "clean"]) w
	# w = (monaco .# "languages.setMonarchTokensProvider" .$! ("clean", cleanMonarchDefinition)) w
	# w = (monaco .# "languages.setLanguageConfiguration" .$! ("clean", cleanMonarchConfig)) w
	# w = (me .# "domEl.style.padding" .= 0) w
	# w = (me .# "domEl.style.display" .= "inline-flex") w
	# w = (me .# "domEl.style.width" .= "100%") w
	# w = (me .# "domEl.style.height" .= "100%") w
	# w = (me .# "domEl.style.overflow" .= "hidden") w
	# (editor,w) = (monaco .# "editor.create" .$ (me .# "domEl", jsRecord
		[ "automaticLayout" :> True
		, "language" :> "clean"
		, "detectIndentation" :> False
		, "fontSize" :> 12
		, "insertSpaces" :> False
		, "lineNumbersMinChars" :> 4
		, "mouseWheelZoom" :> True
		, "renderWhitespace" :> "selection"
		, "roundedSelection" :> False
		, "showUnused" :> True
		, "tabSize" :> 4
		, "value" :> me .# "attributes.contents"
		])) w
	# w = (me .# "editor" .= editor) w
	# w = (editor .# "focus" .$! ()) w

	# (onShow,w) = jsWrapFun onShow me w
	  w = (me .# "onShow" .= onShow) w

	# (onDidChangeModelContent,w) = jsWrapFun onDidChangeModelContent me w
	  w = (me .# "editor.onDidChangeModelContent" .$! onDidChangeModelContent) w

	# w = addEditorAction "soccerfun-save" "Save" [["Mod.CtrlCmd", "Code.KEY_S"]] onSave w
	# w = addEditorAction "soccerfun-compile" "Compile" [["Mod.CtrlCmd", "Code.KEY_U"]] onCompile w

	= w
where
	onShow _ w
		= (me .# "editor.layout" .$! ()) w

	onDidChangeModelContent _ w
		= doEditEvent HasChanges w

	onSave _ w
		# (val,w) = (me .# "editor.getValue" .$? ()) ("", w)
		# w = doEditEvent (SetContent val) w
		= doEditEvent RequestSave w
	onCompile _ w
		= doEditEvent RequestCompile w

	addEditorAction id label keybindings fun w
		# (fun,w) = jsWrapFun fun me w
		# (keybindings,w) = mapSt getKeyBinding keybindings w
		= (me .# "editor.addAction" .$! jsRecord
			[ "id" :> id
			, "label" :> label
			, "keybindings" :> keybindings
			, "run" :> fun
			]) w
	where
		getKeyBinding keys w
			# (vals,w) = mapSt getKey keys w
			= (foldr (bitor) 0 vals, w)
		getKey key w
			# (val,w) = jsGlobal ("monaco.Key"+++key) .? w
			= (fromMaybe (abort "addAction: invalid monaco key") (jsValToInt val), w)

	doEditEvent ev w
		= (me .# "doEditEvent" .$! (me .# "attributes.taskId", me .# "attributes.editorId", toJSON ev)) w
