# SoccerFun

This is an attempt to use [iTasks-Electron][] to create a new, cross-platform
GUI for Peter Achten's [SoccerFun][] project.

## Installation (for usage)

1. Download the package for your operating system and unpack it in the location
  of your choice:

	- [Linux, x64](https://gitlab.science.ru.nl/cstaps/SoccerFun/-/jobs/artifacts/master/raw/dist/soccerfun-linux-x64.tar.gz?job=linux-x64)
	- [Windows, x64](https://gitlab.science.ru.nl/cstaps/SoccerFun/-/jobs/artifacts/master/raw/dist/soccerfun-windows-x64.zip?job=windows-x64)

2. Run the `Start` executable in the unpacked directory.

3. On some systems you may have to set the right permissions of
  `chrome-sandbox`. The application will warn about this when this is the case.
  You can fix the permissions with:

	```bash
	sudo chown root:root chrome-sandbox
	sudo chmod 4755 chrome-sandbox
	```

4. Under File &rarr; Preferences, set the right path to a working Clean
  installation directory.

## Usage

To develop new teams, open the Team Editor (from File). You can create a new
team in the `Teams` directory with the `New` button, and you can also browse
the supporting code in the other directories.

When you have edited a team, compile it (<kbd>F1</kbd> + `compile` or
<kbd>Ctrl-U</kbd>). In the main screen, choose File &rarr; Reload Interpreted
Teams. When you start a new game from Game &rarr; Options (<kbd>Ctrl-O</kbd>),
the new team appears in the list.

## Installation (for development)

1. Install [Node.js][].

2. Download the repository:

	```bash
	git clone --recursive https://gitlab.science.ru.nl/cstaps/SoccerFun
	cd SoccerFun/src
	```

3. Install the dependencies:

	```bash
	npm install
	```

4. Build the project:

	```bash
	cp SoccerFun-linux.prj.default SoccerFun.prj   # or use: SoccerFun-windows.prj.default
	cpm SoccerFun.prj
	```

5. Run `./SoccerFun.exe`.

6. On some systems, you will have to set the right permissions of
  `chrome-sandbox` for electron to work. The application will warn about this
  when this is the case. You can fix the permissions with:

	```bash
	sudo chown root:root node_modules/electron/dist/chrome-sandbox
	sudo chmod 4755 node_modules/electron/dist/chrome-sandbox
	```

[iTasks-Electron]: https://gitlab.com/camilstaps/iTasks-electron/
[Node.js]: https://nodejs.org/en/
[SoccerFun]: http://www.cs.ru.nl/P.Achten/SoccerFun/SoccerFun.html
